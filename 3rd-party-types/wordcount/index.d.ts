declare module 'wordcount' {
  /**
   * Count the words in a string.
   */
  function wordcount(str: string): number;
  export = wordcount;
}
