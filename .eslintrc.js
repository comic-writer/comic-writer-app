module.exports = {
  extends: [
    // general checks
    "eslint:recommended",
    // react
    "plugin:react/recommended",
    "plugin:eslint-plugin-react-hooks/recommended",
    "plugin:jsx-a11y/recommended",
    // typescript
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    // imports
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:import/typescript",
  ],
  rules: {
    // A temporary hack related to IDE not resolving correct package.json
    'import/no-extraneous-dependencies': 'off',
    // Disallow usage before defintions except for functions because they're hoisted
    "@typescript-eslint/no-use-before-define": ["error", "nofunc"],
    // We use typescript to define and check props types
    "react/prop-types": "off",
    // This is a judgement call
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    // allow leaving file extensions off of import statements
    "import/extensions": "off",
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
    createDefaultProgram: true,
  },
  settings: {
    'import/resolver': {
      // See https://github.com/benmosher/eslint-plugin-import/issues/1396#issuecomment-575727774 for line below
      node: {},
      webpack: {
        config: require.resolve('./configs/webpack.config.eslint.js'),
      },
      "typescript": {
        // always try to resolve types under `<root>@types` directory even it doesn't contain any source code, like `@types/unist`
        "alwaysTryTypes": true
      },
    },
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    react: {
      // Tells eslint-plugin-react to automatically detect the version of React
      version: "detect"
    },
  },
};
