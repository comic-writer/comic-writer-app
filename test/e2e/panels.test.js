import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, pathToIndexPage } from './helpers';

fixture('panels')
  .page(pathToIndexPage());

test('one panel', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      ''
    ]);
});

test('two panels', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.panel)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      'Panel 2',
      ''
    ]);
});

test('insert panel between existing panels', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.panel)
    .pressKey('up')
    .pressKey('enter')
    .pressKey('up')
    .pressKey(shortcuts.panel)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      'Panel 2',
      '',
      'Panel 3',
      ''
    ]);
});
