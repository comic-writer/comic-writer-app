/*
 * Keyboard shortcuts for sending to Testcafe's `t.pressKey()`
 */

export const page = 'meta+1';
export const panel = 'meta+2';
export const dialogue = 'meta+3';
export const caption = 'meta+4';
export const sfx = 'meta+5';
export const bold = 'meta+b';

export const undo = 'meta+z';
export const redo = 'shift+meta+z';
