import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, getSelectedText, lettering, pathToIndexPage } from './helpers';

fixture('bold shortcut')
  .page(pathToIndexPage());

test('lettering with content selected', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .doubleClick(selectors.css.letteringContentToken)
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **content**')
    ])
    .expect(getSelectedText()).eql('content');
});

test('lettering with partial content selected', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: cont**ent**')
    ])
    .expect(getSelectedText()).eql('ent')
});

test('lettering with no content selected', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: content****')
    ])
    // hacky way to check that cursor was between the star pairs
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: conten**')
    ])
});

test('lettering with no content', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .pressKey('backspace')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: ****')
    ])
    // hacky way to check that cursor was between the star pairs
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION:**')
    ])
});

test('lettering with cursor in middle of a word', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: cont****ent')
    ])
    // hacky way to check that cursor was between the star pairs
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: con**ent')
    ])
});

test('lettering with repeated uses', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .doubleClick(selectors.css.letteringContentToken)
    .pressKey(shortcuts.bold)
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: ****content****')
    ])
    .expect(getSelectedText()).eql('content')
});

test('undoing bold command', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .doubleClick(selectors.css.letteringContentToken)
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **content**')
    ])
    .expect(getSelectedText()).eql('content')
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: content')
    ])
    .expect(getSelectedText()).eql('content')
});

test('paragraph with word selected', async t => {
  await t
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '**blah**'
    ])
    .expect(getSelectedText()).eql('blah')
});

test('paragraph with word partially selected', async t => {
  await t
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'bl**ah**'
    ])
    .expect(getSelectedText()).eql('ah')
});

test('paragraph with nothing selected', async t => {
  await t
    .typeText(selectors.editorInput(), 'blah')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'blah****'
    ])
    // hacky way to check that cursor was between the star pairs
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'bla**'
    ])
});

test('on an empty line', async t => {
  await t
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('enter')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'blah',
      '****'
    ])
    // hacky way to check that cursor was between the star pairs
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'blah**'
    ])
});

test('cannot bold a page line', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey('up')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      ''
    ])
});

test('cannot bold a panel line', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey('up')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey('shift+right')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      ''
    ])
});

test('cannot bold across multiple lines', async t => {
  await t
    .typeText(selectors.editorInput(), 'one')
    .pressKey('enter')
    .typeText(selectors.editorInput(), 'two')
    .pressKey('enter')
    .pressKey('up')
    .pressKey('shift+up')
    .pressKey('shift+up')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'one',
      'two',
      ''
    ])
});

test('cannot bold in a metadata line', async t => {
  await t
    .typeText(selectors.editorInput(), 'key: value')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'key: value'
    ])
});

test('cannot bold in lettering meta', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'foo')
    // move to the left of the :
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    // select some meta tokens
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    // try to bold them
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: foo')
    ])
});

test('cannot bold if part of selection is in lettering meta', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'foo')
    // move to the left of the :
    .pressKey('left')
    .pressKey('left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    // select some meta tokens
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    // try to bold them
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: foo')
    ])
});
