import 'mobx-react-lite/batchingForReactDom';
import { configure } from 'mobx';

configure({
  /**
   * Setting to 'observed' means observables that have observers can only be
   * updated in mobx actions. When initializing observables e.g. in an class
   * constructor, there aren't any observers yet so those assignments don't
   * need to be in mobx actions.
   *
   * https://github.com/mobxjs/mobx/issues/563
   * https://github.com/mobxjs/mobx/issues/1507
   * https://mobx.js.org/refguide/api.html#enforceactions
   */
  enforceActions: 'observed'
});
