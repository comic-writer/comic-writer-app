import { StrictIpcRenderer } from '../ipc/types';
import { RootStore } from '.';

/**
 * Wire up the store to IpcRenderer to receive messages from main process.
 *
 * @param store
 * @param ipcRenderer
 */
export function connectIpc(store: RootStore, ipcRenderer: StrictIpcRenderer) {
  ipcRenderer.on('undo', () => {
    store.editor.runCommand('undo');
  });

  ipcRenderer.on('redo', () => {
    store.editor.runCommand('redo');
  });

  ipcRenderer.on('selectAll', () => {
    store.editor.runCommand('selectAll');
  });

  ipcRenderer.on('save-as:request', () => {
    ipcRenderer.send('save-as:response', store.editor.status);
  });

  ipcRenderer.on('save:request', () => {
    ipcRenderer.send('save:response', store.editor.status);
  });

  ipcRenderer.on('save:result', (_, result) => {
    if (result.success) {
      store.editor.saveSucceeded(result.filePath);
    } else {
      console.error(`save failed because ${result.errorMessage}`);
    }
  });

  ipcRenderer.on('new:request', () => {
    ipcRenderer.send('new:response', store.editor.status);
  });

  ipcRenderer.on('new:result', () => {
    store.editor.newScript();
  });

  ipcRenderer.on('open:request', () => {
    ipcRenderer.send('open:response', store.editor.status);
  });

  ipcRenderer.on('open:result', (_, result) => {
    if (result.success) {
      store.editor.openScript(result.filePath, result.source);
    } else {
      console.error(`open failed because ${result.errorMessage}`);
    }
  });

  ipcRenderer.on('export-pdf:request', () => {
    ipcRenderer.send('export-pdf:response', {
      script: store.script.fullScript,
      filePath: store.editor.filePath
    });
  });
}
