import './configure-mobx';
import { ScriptStore } from '../script';
import { EditorStore } from '../editor';
export { connectIpc } from './connect-renderer-ipc';

export interface RootStore {
  script: ScriptStore;
  editor: EditorStore;
}

export function createStore(): RootStore {
  const scriptStore = new ScriptStore();
  const rootStore = {
    script: scriptStore,
    editor: new EditorStore(scriptStore),
  };

  if (process.env.NODE_ENV !== 'production' || process.env.E2E_BUILD === 'true') {
    exposeGlobalHooks(rootStore);
  }

  return rootStore;
}

function exposeGlobalHooks(store: RootStore): void {
  const win = window as unknown as {
    openScript: (filePath: string, source: string) => void;
  };

  // add hook so tests can preload a script
  win.openScript = (filePath: string, source: string) => {
    if (typeof filePath !== 'string' || typeof source !== 'string') {
      throw new Error('Signature is: openScript(filePath: string, source: string)');
    }
    store.editor.openScript(filePath, source);
  }
}
