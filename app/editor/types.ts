// events from Editor

export interface EditorChangeEvent {
  /** New contents of the editor */
  lines: Array<string>;
}

export interface ScrollPosition {
  /**
   * Zero-based line number of the line that the editor is on. This is not
   * necessarily the top visible line.
   */
  currentLine: number;
}

export type EditorScrollEvent = ScrollPosition;

// Prop types and events for Outline component

export interface OutlineItem {
  id: string;
  type: string;
  lineNumber: number;
  current: boolean;
}

export interface SpreadOutlineItem extends OutlineItem {
  type: 'spread';
  label: string;
}

export interface PanelOutlineItem extends OutlineItem {
  type: 'panel';
  panelNumber: number;
  description: string | null;
}

export interface OutlineItemSelectionEvent {
  /** Zero-based line number of the selected item */
  lineNumber: number;
}

/**
 * Fired by outline items when they need to be centered in the outline.
 */
export interface CenteringRequestEvent {
  /** The element to be centered */
  element: HTMLElement;
}

/**
 * Commands that the editor can perform.
 */
export interface EditorCommand {
  name: TextEditorCommand;
}

/**
 *
 * Note: Every command isn't in here (e.g. cut) because the native electron
 * version of those works fine. Only the commands that have to be run by
 * CodeMirror are here.
 */
export type TextEditorCommand =
  // Generic text editor commands
  'undo' |
  'redo' |
  'selectAll' |
  // CW-specific commands
  'insertPage' |
  'insertPanel' |
  'dialogue' |
  'caption' |
  'sfx' |
  'bold'
;

export interface ScriptReplacement {
  filePath: string | null;
  source: string;
}
