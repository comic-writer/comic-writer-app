import React, { useCallback } from 'react';
import { observer } from 'mobx-react-lite';

import { EditorChangeEvent, EditorScrollEvent } from '../types';

import { useStore } from '../../store/use-store';
import CodeMirror from './codemirror/CodeMirror';

export const TextEditor = observer(() => {
  const {
    script: scriptStore,
    editor: editorStore
  } = useStore();

  const onChange = useCallback((event: EditorChangeEvent) => {
    editorStore.updateScript(event.lines);
  }, [editorStore]);

  const onScroll = useCallback((event: EditorScrollEvent) => {
    editorStore.updateScroll(event);
  }, [editorStore]);

  return (
    <CodeMirror
      key={editorStore.key}
      controlledValue={editorStore.controlledValue}
      targetLine={editorStore.targetLine}
      command={editorStore.command}
      panelCounts={scriptStore.panelCounts}
      wordCounts={scriptStore.wordCounts}
      characters={scriptStore.speakers}
      onChange={onChange}
      onScroll={onScroll}
    />
  );
});
