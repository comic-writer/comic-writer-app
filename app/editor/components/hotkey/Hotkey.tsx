import React from 'react';
import './Hotkey.css';
// 'Cmd-B'
// 'Ctrl-B

const parseHotkeyToJsx = (keyCombo: string) =>
    // Make array, and split on `-`
    keyCombo.split('-')
    // Remove "Command" and replace it with a squeeedelyboo-boi '⌘'
    .map(key => key.replace('Cmd', '⌘'))
    // make "keys".
    .map((key, i) => <span key={i} className="c-hotkey__key">{key}</span>) // Decorate with JSX
    // insert + JSX between the fence posts, except for the last
    .reduce((result, key, index, arr) =>
      result.concat(
        key,
        (index !== arr.length - 1) && <span key={index + 10}>+</span>
      ),
      [] as Array<JSX.Element | boolean>
    )

type HotkeyProps = {
  keyCombo: string;
};

const Hotkey = (props: HotkeyProps) =>
  <div className="c-hotkey">
    {parseHotkeyToJsx(props.keyCombo)}
  </div>;

export {
  Hotkey
}

// ⌘
// Ctrl
// Alt
