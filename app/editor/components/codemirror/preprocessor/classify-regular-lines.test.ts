import classifyLines from './classify';
import { LineClassification } from './types';

type TestCase = [string, number, LineClassification];

describe('classify regular lines', () => {
  describe('single line edit', () => {
    let classifier: (line: string, lineNumber: number) => LineClassification;

    beforeEach(() => {
      classifier = classifyLines(3, 1, 0);
    });

    const testCases: Array<TestCase> = [
      // just some random whatever lines
      ['pag', 3, { type: 'regular', frozen: false }],
      ['pagea', 3, { type: 'regular', frozen: false }],
      ['pagesa', 3, { type: 'regular', frozen: false }],
      ['spread', 3, { type: 'regular', frozen: false }],

      // other types of comic nodes
      ['Just a regular paragraph line', 3, { type: 'regular', frozen: false }],
      ['key: value', 3, { type: 'regular', frozen: false }],
      ['\tCAPTION: We lived that day.', 3, { type: 'regular', frozen: false }],
      ['\tSFX: BLAM', 3, { type: 'regular', frozen: false }],
      ['\tWONKA: You lose!', 3, { type: 'regular', frozen: false }],
      ['', 3, { type: 'regular', frozen: false }],
      [' ', 3, { type: 'regular', frozen: false }],
    ];

    testCases
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}"`, () => {
          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });
});
