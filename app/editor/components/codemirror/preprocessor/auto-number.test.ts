import createAutoNumber from './auto-number';
import { LineClassification } from './types';

describe('auto number', () => {
  describe('single page', () => {
    it('nothing frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'single-page', frozen: false },
        { type: 'regular', frozen: false },
        { type: 'single-page', frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page',
        'blah',
        'page',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Page 1',
        'blah',
        'Page 2',
        'foo'
      ]);
    });

    it('first page frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'single-page', frozen: true },
        { type: 'regular', frozen: false },
        { type: 'single-page', frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page',
        'blah',
        'page',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'page',
        'blah',
        'Page 2',
        'foo'
      ]);
    });
  })

  describe('2 page spread', () => {
    it('nothing frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'multi-page', count: 2, frozen: false },
        { type: 'regular', frozen: false  },
        { type: 'multi-page', count: 2, frozen: false  },
        { type: 'regular', frozen: false  },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1-2',
        'blah',
        'page 3-4',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Pages 1-2',
        'blah',
        'Pages 3-4',
        'foo'
      ]);
    });

    it('first spread frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'multi-page', count: 2, frozen: true },
        { type: 'regular', frozen: false },
        { type: 'multi-page', count: 2, frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1-2',
        'blah',
        'page 3-4',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'page 1-2',
        'blah',
        'Pages 3-4',
        'foo'
      ]);
    });
  })

  describe('big multi page spread', () => {
    it('nothing frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'multi-page', count: 3, frozen: false },
        { type: 'regular', frozen: false },
        { type: 'multi-page', count: 4, frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1-3',
        'blah',
        'page 4-7',
        'foo',
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Pages 1-3',
        'blah',
        'Pages 4-7',
        'foo'
      ]);
    });

    it('first spread frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'multi-page', count: 3, frozen: true },
        { type: 'regular', frozen: false },
        { type: 'multi-page', count: 4, frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1-3',
        'blah',
        'page 4-7',
        'foo',
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'page 1-3',
        'blah',
        'Pages 4-7',
        'foo'
      ]);
    });
  })

  describe('kitchen sink', () => {
    it('nothing frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'single-page', frozen: false },
        { type: 'regular', frozen: false },
        { type: 'partial-page-range', frozen: false },
        { type: 'multi-page', count: 2, frozen: false },
        { type: 'regular', frozen: false },
        { type: 'multi-page', count: 4, frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1',
        'blah',
        'page 2-',
        'page 3-4',
        'foo',
        'page 5-8',
        'bar'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Page 1',
        'blah',
        'Pages 2-',
        'Pages 3-4',
        'foo',
        'Pages 5-8',
        'bar'
      ]);
    });

    it('first line frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'single-page', frozen: true },
        { type: 'regular', frozen: false },
        { type: 'partial-page-range', frozen: false },
        { type: 'multi-page', count: 2, frozen: false },
        { type: 'regular', frozen: false },
        { type: 'multi-page', count: 4, frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page',
        'blah',
        'page 2-',
        'page 3-4',
        'foo',
        'page 5-8',
        'bar'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'page',
        'blah',
        'Pages 2-',
        'Pages 3-4',
        'foo',
        'Pages 5-8',
        'bar'
      ]);
    });
  })

  describe('partial page range', () => {
    it('nothing frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'partial-page-range', frozen: false },
        { type: 'regular', frozen: false },
        { type: 'partial-page-range', frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1-',
        'blah',
        'page 2-',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Pages 1-',
        'blah',
        'Pages 2-',
        'foo'
      ]);
    });

    it('first line frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'partial-page-range', frozen: true },
        { type: 'regular', frozen: false },
        { type: 'partial-page-range', frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'page 1-',
        'blah',
        'page 2-',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'page 1-',
        'blah',
        'Pages 2-',
        'foo'
      ]);
    });
  })

  describe('multi page with count less than 1', () => {
    it('outputs as-is', () => {
      const classifications: Array<LineClassification> = [
        { type: 'multi-page', count: 0, frozen: false },
        { type: 'regular', frozen: false },
        { type: 'multi-page', count: -1, frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'Pages 2-2',
        'blah',
        'Pages 2-1',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Pages 2-2',
        'blah',
        'Pages 2-1',
        'foo'
      ]);
    });
  })

  describe('single panel', () => {
    it('nothing frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'panel', frozen: false },
        { type: 'regular', frozen: false },
        { type: 'panel', frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'panel',
        'blah',
        'panel',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'Panel 1',
        'blah',
        'Panel 2',
        'foo'
      ]);
    });

    it('first panel frozen', () => {
      const classifications: Array<LineClassification> = [
        { type: 'panel', frozen: true },
        { type: 'regular', frozen: false },
        { type: 'panel', frozen: false },
        { type: 'regular', frozen: false },
      ];
      const autoNumber = createAutoNumber(classifications);

      const result = [
        'panel',
        'blah',
        'panel',
        'foo'
      ]
        .map(autoNumber);

      expect(result).toEqual([
        'panel',
        'blah',
        'Panel 2',
        'foo'
      ]);
    });
  });
});
