import classifyLines from './classify';
import { LineClassification } from './types';

type TestCase = [string, number, LineClassification];

describe('classify page lines', () => {
  describe('cursor on same line', () => {
    cursorOnSameLineTestCases(3)
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}"`, () => {
          const classifier = classifyLines(3, 1, 0);

          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });

  describe('cursor on same line due to line offset', () => {
    cursorOnSameLineTestCases(2)
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}"`, () => {
          const classifier = classifyLines(3, 1, 1);

          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });

  describe('cursor on different line', () => {
    cursorOnDifferentLineTestCases(2)
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}"`, () => {
          const classifier = classifyLines(3, 1, 0);

          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });

  describe('cursor on different line due to line offset', () => {
    cursorOnDifferentLineTestCases(2)
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}"`, () => {
          const classifier = classifyLines(2, 1, 1);

          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });
});

function cursorOnSameLineTestCases(lineNumber: number): Array<TestCase> {
  return [
    ['page', lineNumber, { type: 'single-page', frozen: true }],
    ['pages', lineNumber, { type: 'multi-page', count: 2, frozen: true }],
    ['page  ', lineNumber, { type: 'single-page', frozen: true }],
    ['pages  ', lineNumber, { type: 'multi-page', count: 2, frozen: true }],
    ['page 2', lineNumber, { type: 'single-page', frozen: true }],
    ['page 20', lineNumber, { type: 'single-page', frozen: true }],
    ['pages 2', lineNumber, { type: 'single-page', frozen: true }],
    ['pages 20', lineNumber, { type: 'single-page', frozen: true }],
    ['page  2', lineNumber, { type: 'single-page', frozen: true }],
    ['page   20', lineNumber, { type: 'single-page', frozen: true }],
    ['page 1-2', lineNumber, { type: 'multi-page', count: 2, frozen: true }],
    ['pages 1-2', lineNumber, { type: 'multi-page', count: 2, frozen: true }],
    ['page  2-5', lineNumber, { type: 'multi-page', count: 4, frozen: true }],
    ['pages  2-5', lineNumber, { type: 'multi-page', count: 4, frozen: true }],
    ['page 20-22', lineNumber, { type: 'multi-page', count: 3, frozen: true }],
    ['pages 20-22', lineNumber, { type: 'multi-page', count: 3, frozen: true }],

    // invalid range cases
    ['pages 22-10', lineNumber, { type: 'invalid-page-range', frozen: true }],
    ['pages 5-5', lineNumber, { type: 'invalid-page-range', frozen: true }],

    // partial range cases
    ['page 2-', lineNumber, { type: 'partial-page-range', frozen: true }],
    ['pages 2-', lineNumber, { type: 'partial-page-range', frozen: true }],
    ['page 20-', lineNumber, { type: 'partial-page-range', frozen: true }],
    ['pages 20-', lineNumber, { type: 'partial-page-range', frozen: true }],
  ];
}

function cursorOnDifferentLineTestCases(lineNumber: number): Array<TestCase> {
  return [
    ['page', lineNumber, { type: 'single-page', frozen: false }],
    ['pages', lineNumber, { type: 'multi-page', count: 2, frozen: false }],
    ['page  ', lineNumber, { type: 'single-page', frozen: false }],
    ['pages  ', lineNumber, { type: 'multi-page', count: 2, frozen: false }],

    ['page 2', lineNumber, { type: 'single-page', frozen: false }],
    ['page 20', lineNumber, { type: 'single-page', frozen: false }],
    ['pages 2', lineNumber, { type: 'single-page', frozen: false }],
    ['pages 20', lineNumber, { type: 'single-page', frozen: false }],
    ['page  2', lineNumber, { type: 'single-page', frozen: false }],
    ['page   20', lineNumber, { type: 'single-page', frozen: false }],

    ['page 1-2', lineNumber, { type: 'multi-page', count: 2, frozen: false }],
    ['pages 1-2', lineNumber, { type: 'multi-page', count: 2, frozen: false }],
    ['page  2-5', lineNumber, { type: 'multi-page', count: 4, frozen: false }],
    ['pages  2-5', lineNumber, { type: 'multi-page', count: 4, frozen: false }],
    ['page 20-22', lineNumber, { type: 'multi-page', count: 3, frozen: false }],
    ['pages 20-22', lineNumber, { type: 'multi-page', count: 3, frozen: false }],

    // invalid range with cursor gone becomes a 2 pager
    ['pages 22-10', lineNumber, { type: 'multi-page', count: 2, frozen: false }],
    // same to same range is a 1 pager
    ['pages 5-5', lineNumber, { type: 'single-page', frozen: false }],

    // partial range with cursor gone, counts as a single page
    ['page 2-', lineNumber, { type: 'single-page', frozen: false }],
    ['pages 2-', lineNumber, { type: 'single-page', frozen: false }],
    ['page 20-', lineNumber, { type: 'single-page', frozen: false }],
    ['pages 20-', lineNumber, { type: 'single-page', frozen: false }],
  ];
}
