import { Editor } from 'codemirror';
import { ensureCursorAtLineStart } from './command-helpers';

/**
 * Insert a page.
 *
 * @param cm
 */
export function insertPage(cm: Editor) {
  cm.operation(() => {
    ensureCursorAtLineStart(cm, '+insertPage');
    cm.replaceRange('page\n', cm.getCursor(), undefined, '+insertPage');
  });
}
