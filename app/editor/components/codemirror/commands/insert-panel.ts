import { Editor } from 'codemirror';
import { ensureCursorAtLineStart } from './command-helpers';

/**
 * Insert a page.
 *
 * @param cm
 */
export function insertPanel(cm: Editor) {
  cm.operation(() => {
    ensureCursorAtLineStart(cm, '+insertPanel');
    cm.replaceRange('panel\n', cm.getCursor(), undefined, '+insertPanel');
  });
}
