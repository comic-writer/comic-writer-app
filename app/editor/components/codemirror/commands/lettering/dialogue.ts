import { Editor } from 'codemirror';
import { ensureCursorOnBlankLine } from '../command-helpers';
import { trigger } from './lettering-snippet';
import { createDialogueTabStops } from './tab-stops';

const DEFAULT_SUBJECT = 'CHARACTER';
const ORIGIN = '+insertDialogue';

export function create(getCharacterNames: () => Array<string>) {
  return function dialogue(cm: Editor) {
    // make space for the lettering
    ensureCursorOnBlankLine(cm, ORIGIN);

    trigger(cm, {
      template: `\t${DEFAULT_SUBJECT} (): content`,
      origin: ORIGIN,
      tabStops: createDialogueTabStops(getCharacterNames, DEFAULT_SUBJECT)
    });
  };
}
