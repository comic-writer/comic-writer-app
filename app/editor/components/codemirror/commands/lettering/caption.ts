import { Editor } from 'codemirror';
import { ensureCursorOnBlankLine } from '../command-helpers';
import { trigger } from './lettering-snippet';
import { createStaticSubjectTabStops } from './tab-stops';

const ORIGIN = '+insertCaption';
const SUBJECT = 'CAPTION';

export function caption(cm: Editor) {
  // make space for the lettering
  ensureCursorOnBlankLine(cm, ORIGIN);

  trigger(cm, {
    template: `\t${SUBJECT} (): content`,
    origin: ORIGIN,
    tabStops: createStaticSubjectTabStops(SUBJECT, ORIGIN)
  });
}
