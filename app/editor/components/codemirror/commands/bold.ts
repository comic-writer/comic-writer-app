import CodeMirror, { Token, Editor, Range } from 'codemirror';
import { wrapSelection } from '../helpers';
import { LETTERING_CONTENT, PARAGRAPH, LETTERING_LINE } from '../mode/token';

const STAR_STAR = '**';

/**
 * CodeMirror command that inserts bold stars.
 *
 * @param cm CodeMirror Editor
 */
export function boldCommand(cm: CodeMirror.Editor) {
  const selection = cm.listSelections()[0];

  if (!selectionCanBeBolded(cm, selection)) {
    return;
  }

  const lineNumber = cm.getCursor().line;

  const originalContent = cm.getLine(lineNumber);
  const newContent = wrapSelection(
    originalContent,
    selection.from().ch,
    selection.to().ch,
    STAR_STAR,
    STAR_STAR
  );

  cm.operation(() => {
    cm.replaceRange(newContent, {
      ch: 0,
      line: lineNumber
    }, {
      ch: originalContent.length,
      line: lineNumber
    });

    cm.setSelection({
      ch: selection.from().ch + STAR_STAR.length,
      line: lineNumber
    }, {
      ch: selection.to().ch + STAR_STAR.length,
      line: lineNumber
    });
  });
}

function selectionCanBeBolded(cm: Editor, selectedRange: Range): boolean {
  const selection = {
    from: selectedRange.from(),
    to: selectedRange.to(),
    empty: selectedRange.empty()
  };

  // Can't bold when there's a multi line selection
  if (selection.from.line !== selection.to.line) {
    return false;
  }

  const lineNumber = selection.from.line;
  const lineTokens = cm.getLineTokens(lineNumber);

  // just a cursor, no selection
  if (selection.empty) {
    const line = cm.getLine(lineNumber);

    // can always bold on a blank line
    if (line === '') {
      return true;
    }

    // handle when cursor is in lettering content area but there's no content
    if (isLetteringLine(lineTokens)) {
      const colonIndex = line.indexOf(':');
      if (selection.from.ch === line.length && colonIndex !== -1) {
        return true;
      }
    }

    const tokenType = cm.getTokenTypeAt(selection.from);
    const nextTokenType = cm.getTokenTypeAt({
      ...selection.from,
      ch: selection.from.ch + 1
    });

    return tokenType == null
      ? isBoldable(nextTokenType)
      : isBoldable(tokenType);
  } else {
    return lineTokens
      // filter down to selected tokens
      .filter(token => token.end > selection.from.ch && token.start < selection.to.ch)
      .every(token => isBoldable(token.type));
  }
}

function isBoldable(tokenType: string | null): boolean {
  if (tokenType == null) return false;
  if (tokenType.includes(LETTERING_CONTENT)) return true;
  if (tokenType.includes(PARAGRAPH)) return true;

  return false;
}

function isLetteringLine(tokens: Array<Token>): boolean {
  return tokens
    .some(token => (token.type || '').includes(LETTERING_LINE))
}
