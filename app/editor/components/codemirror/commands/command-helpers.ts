import { Editor } from 'codemirror';

/**
 * If the cursor isn't already at the start of a line, insert a newline at the
 * cursor.
 *
 * @param cm editor
 * @param origin Used for CodeMirror's undo/redo tracking purposes
 */
export function ensureCursorAtLineStart(cm: Editor, origin?: string) {
  const cursor = cm.getCursor();

  if (cursor.ch !== 0) {
    cm.replaceRange('\n', cursor, undefined, origin);
  }
}

export function ensureCursorOnBlankLine(cm: Editor, origin?: string) {
  ensureCursorAtLineStart(cm, origin);

  const cursor = cm.getCursor();
  const line = cm.getLine(cursor.line);

  if (line !== '') {
    // newline at front of line
    cm.replaceRange('\n', {
      line: cursor.line,
      ch: 0
    }, undefined, origin);

    // move cursor to newly created blank line
    cm.setCursor(cursor.line, 0, {
      origin
    });
  }
}
