import React, { useCallback } from 'react';
import { Hotkey } from './hotkey/Hotkey';
import { ToolBar } from './tool-bar/ToolBar';
import ToolTip from '../../components/tool-tip/ToolTip'
import { Hotkeys } from '../../hotkeys'
import { observer } from 'mobx-react-lite';
import { useStore } from '../../store/use-store';
import { TextEditorCommand } from '../types';

/**
 * ToolBar that sits above the editor.
 */
export const EditorToolBar = observer(() => {
  const { editor: editorStore } = useStore();

  const onClick = useCallback((commandName: TextEditorCommand) => {
    editorStore.runCommand(commandName);
  }, [editorStore]);
  
  return (
    <ToolBar.Container>
      {Hotkeys
        .filter(hotkey => hotkey.inToolbar)
        .map((hotkey, i) =>
          <ToolTip key={i}>
            <Hotkey keyCombo={hotkey.hotkey} />
            <ToolBar.Button onClick={() => onClick(hotkey.command)}>
              {hotkey.name}
            </ToolBar.Button>
          </ToolTip>)}
    </ToolBar.Container>
  );
});
