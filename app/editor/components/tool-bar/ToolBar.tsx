import React, { MouseEventHandler } from 'react';
import { WithReactChildren } from '../../../components/types';
import './ToolBar.css';
import './ToolBarButton.css';


type ContainerProps = WithReactChildren;

const Container = (props: ContainerProps) =>
  <div className="c-toolbar">{props.children}</div>;

type ButtonProps = {
  onClick: MouseEventHandler;
} & WithReactChildren;

const Button = (props: ButtonProps) =>
  <button className={`c-toolbar-button`} onClick={props.onClick}>
    {props.children}
  </button>;

const ToolBar = {
  Button: Button,
  Container: Container
}
export {
  ToolBar
}
