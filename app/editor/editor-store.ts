import {
  observable,
  action,
  computed,
  decorate,
} from 'mobx';

import { NO_TARGET_LINE } from './constants';
import {
  ScrollPosition,
  SpreadOutlineItem,
  PanelOutlineItem,
  TextEditorCommand,
  EditorCommand,
  ScriptReplacement,
} from './types';
import {
  Spread,
  Panel,
} from '../parser/types';
import { ScriptStore } from '../script';
import * as parts from '../comic-part-types';
import { EditorStatus } from '../ipc/types';

/**
 *
 * Important: Anything in this class that should be enhanced by mobx must also
 * be part of the decorate block below the class definition.
 */
export class EditorStore {
  /** Zero-based line number that the editor is on. */
  currentLine: number;
  /** Zero-based line number that the editor needs to scroll to. */
  targetLine: number;
  /**
   * Command for editor to run. When this changes to a non-null value the editor
   * runs the command once.
   */
  command: EditorCommand | null;

  /** Source of truth for script content. */
  scriptStore: ScriptStore;

  /** Path where the script was last saved, if at all.  */
  filePath: string | null;
  /** Has script content changed since the last save? */
  dirty: boolean;

  /**
   * The script content source of truth is managed by the editor component but
   * this can be changed to force the script content to a specific value.
   *
   * The value is "boxed" in an object so it can be set to the same value as
   * before and still cause a change in the editor.
   */
  controlledValue: {
    value: string;
  };

  /**
   * Key for react rendering purposes. This is changed when a whole new instance
   * of the editor is needed.
   */
  key: number;

  constructor(scriptStore: ScriptStore) {
    this.key = 0;
    this.currentLine = 0;
    this.targetLine = NO_TARGET_LINE;
    this.scriptStore = scriptStore;
    this.command = null;

    this.filePath = null;
    this.dirty = false;
    this.controlledValue = { value: '' };
  }

  // computed values

  get outlineItems(): Array<SpreadOutlineItem | PanelOutlineItem> {
    const items: Array<SpreadOutlineItem | PanelOutlineItem> = [];

    for (const spread of this.scriptStore.locatedSpreads) {
      items.push({
        id: spread.id,
        type: 'spread',
        label: spread.label,
        lineNumber: spread.lineNumber,
        current: spread.id === this.currentItemId
      });

      const panels = spread.children
        .filter((child): child is Panel => child.type === parts.PANEL);

      for (const panel of panels) {
        items.push({
          id: panel.id,
          type: 'panel',
          panelNumber: panel.number,
          lineNumber: panel.lineNumber,
          current: panel.id === this.currentItemId,
          description: panel.description
        });
      }
    }

    return items;
  }

  get topOutlineItem(): SpreadOutlineItem {
    return {
      id: 'top',
      type: 'spread',
      label: 'Top',
      lineNumber: 0,
      current: this.currentItemId == null
    };
  }

  get currentSpread(): Spread | null {
    let currentSpread: Spread | null = null;

    for (const spread of this.scriptStore.locatedSpreads) {
      if (spread.lineNumber <= this.currentLine) {
        currentSpread = spread;
      } else {
        break;
      }
    }

    return currentSpread;
  }

  get currentItemId(): string | null {
    return this.currentPanelId || this.currentSpreadId || null;
  }

  get currentSpreadId(): string | null {
    return this.currentSpread ? this.currentSpread.id : null;
  }

  get currentPanelId(): string | null {
    let currentPanel: Panel | null = null;

    if (this.currentSpread) {
      for (const child of this.currentSpread.children) {
        if (child.lineNumber <= this.currentLine) {
          if (child.type === parts.PANEL) {
            currentPanel = child;
          }
        } else {
          break;
        }
      }
    }

    return currentPanel ? currentPanel.id : null;
  }

  get status(): EditorStatus {
    return {
      filePath: this.filePath,
      source: this.scriptStore.source,
      dirty: this.dirty
    };
  }

  // actions

  updateScroll(scrollWindow: ScrollPosition): void {
    this.currentLine = scrollWindow.currentLine;
    this.targetLine = NO_TARGET_LINE;
  }

  selectOutlineItem(lineNumber: number): void {
    this.targetLine = lineNumber;
  }

  runCommand(name: TextEditorCommand): void {
    this.command = {
      name
    };
  }

  saveSucceeded(filePath: string): void {
    this.filePath = filePath;
    this.dirty = false;
  }

  newScript(): void {
    this.replaceScript({
      filePath: null,
      source: ''
    });
  }

  openScript(filePath: string, source: string): void {
    this.replaceScript({
      filePath,
      source
    });
  }

  updateScript(source: string | string[]): void {
    this.scriptStore.updateScript(source);
    this.dirty = true;
  }

  // helpers

  private replaceScript(script: ScriptReplacement): void {
    this.controlScriptValue(script.source);
    this.scriptStore.updateScript(script.source);
    this.filePath = script.filePath;
    this.dirty = false;
    this.key += 1;
  }

  private controlScriptValue(value: string): void {
    this.controlledValue = {
      value
    };
  }
}

decorate(EditorStore, {
  // observables
  currentLine: observable,
  targetLine: observable,
  command: observable,
  filePath: observable,
  dirty: observable,
  controlledValue: observable.ref,
  key: observable,

  // computeds
  outlineItems: computed,
  currentSpread: computed,
  currentItemId: computed,
  currentPanelId: computed,
  currentSpreadId: computed,
  status: computed,

  // actions
  updateScroll: action,
  selectOutlineItem: action,
  runCommand: action,
  saveSucceeded: action,
  newScript: action,
  openScript: action,
});
