import {
  StrictIpcMain as GenericStrictIpcMain,
  StrictIpcRenderer as GenericStrictIpcRenderer,
} from '@psalaets/typesafe-ipc';
import { FullScript } from '../script/types';

export interface EditorStatus {
  /**
   * Location of the script as of the last successful save, if any. Otherwise
   * null if the script hasn't been saved yet.
   */
  filePath: string | null;
  /**
   * Content of the script.
   */
  source: string;
  /**
   * Has the script been changed since the last save?
   */
  dirty: boolean;
}

interface SaveScriptSuccess {
  /** Was the save successful */
  success: true;
  /** Location the script was saved to. */
  filePath: string;
}

interface SaveScriptFailure {
  /** Was the save successful. */
  success: false;
  /** Actual error message from save failure */
  errorMessage: string;
  /** Human-readadble message about save failure. */
  humanMessage: string;
}

interface OpenScriptSuccess {
  /** Was the open successful */
  success: true;
  /** Content of the script */
  source: string;
  /** Location the script was loaded from */
  filePath: string;
}

interface OpenScriptFailure {
  /** Was the open successful */
  success: false;
  /** Reason why the open failed */
  errorMessage: string;
  /** Human-readable reason for failure */
  humanMessage: string;
}

export interface ExportPdfResponse {
  script: FullScript;
  filePath: string | null;
}

/**
 * Defines the (channel name => payload type) mappings for ipc messages sent
 * through
 *
 * - ipcMain
 * - ipcRenderer
 * - webContents
 */
export type ChannelMap = {
  /**
   * Tells renderer to send back its data related to the current script. Sent
   * from main to renderer to start the save as workflow.
   */
  'open:request': void;
  /**
   * Renderer's response to a "open:request" message. Sent from renderer to
   * main.
   */
  'open:response': EditorStatus;

  'open:result': OpenScriptSuccess | OpenScriptFailure;

  /**
   * Tells renderer to send back its data related to saving the script. Sent
   * from main to renderer to start the save as workflow.
   */
  'save-as:request': void;
  /**
   * Renderer's response to a "save-as:request" message. Sent from renderer to
   * main.
   */
  'save-as:response': EditorStatus;

  /**
   * Tells renderer to send back its data related to saving the script. Sent
   * from main to renderer to start the save workflow.
   */
  'save:request': void;
  /**
   * Renderer's response to a "save:request" message. Sent from renderer to
   * main.
   */
  'save:response': EditorStatus;
  /**
   * The final step in the save workflow which tells the renderer if its script
   * has been successfully saved or not. Sent from main to renderer.
   */
  'save:result': SaveScriptSuccess | SaveScriptFailure;

  /**
   * Tells renderer to send back its data related to starting a new script. Sent
   * from main to renderer to start the new script workflow.
   */
  'new:request': void;
  /**
   * Renderer's response to a "new:request" message. Sent from renderer to
   * main.
   */
  'new:response': EditorStatus;

  /**
   * Tells renderer to clear its script and reset itself.
   */
  'new:result': void;

  /**
   * Tells renderer to send back the script and filepath for pdf generation.
   */
  'export-pdf:request': void;

  /**
   * Renderer's response to a 'export-pdf:request' message.
   */
  'export-pdf:response': ExportPdfResponse;

  // Editing commands sent from main to renderer
  undo: void;
  redo: void;
  selectAll: void;

};

export type StrictIpcMain = GenericStrictIpcMain<ChannelMap>;
export type StrictIpcRenderer = GenericStrictIpcRenderer<ChannelMap>;
