import {
  ipcMain as electronIpcMain,
  ipcRenderer as electronIpcRenderer,
  WebContents,
} from 'electron';
import {
  StrictWebContents,
} from '@psalaets/typesafe-ipc';
import {
  ChannelMap,
  StrictIpcRenderer,
  StrictIpcMain,
} from './types';

/**
 * Strictly typed version of electron's ipcMain.
 */
export const ipcMain: StrictIpcMain = electronIpcMain;
/**
 * Strictly typed version of electron's ipcRenderer.
 */
export const ipcRenderer: StrictIpcRenderer = electronIpcRenderer;

/**
 * Returns a strictly typed version of a BrowserWindow's WebContents.
 */
export function asStrict(webContents: WebContents): StrictWebContents<ChannelMap> {
  return webContents;
}
