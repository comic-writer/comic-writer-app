import React from 'react';

import './Writer.css';

import { SplitView } from '../components/split-view/SplitView';
import { TextEditor } from '../editor/components/TextEditor';
import { EditorToolBar } from '../editor/components/EditorToolBar';
import { WiredOutline as Outline } from '../editor/components/outline/Outline';

export const Writer = () => {
  return (
    <div className="c-writer">
      <SplitView minSize={250}>
        <Outline />
        <div className="c-writer__editor">
          <div className="c-writer__toolbar">
            <EditorToolBar />
          </div>
          <div className="c-writer__text-editor">
            <TextEditor />
          </div>
        </div>
      </SplitView>
    </div>
  );
};
