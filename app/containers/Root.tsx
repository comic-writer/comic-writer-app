import React from 'react';
import { HashRouter } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import { createStore } from '../store';
import Routes from '../Routes';
import { RootStoreContext } from '../store/root-store-context';

type Props = {
  store: ReturnType<typeof createStore>;
};

const Root = ({ store }: Props) => (
  <HashRouter>
    <RootStoreContext.Provider value={store}>
      <Routes />
    </RootStoreContext.Provider>
  </HashRouter>
);

export default hot(Root);
