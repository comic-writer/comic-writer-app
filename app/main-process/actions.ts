import {
  dialog,
  BrowserWindow,
  shell,
  SaveDialogReturnValue,
} from 'electron';
import path from 'path';
import { EditorStatus, ExportPdfResponse } from '../ipc/types';
import { asStrict, ipcMain } from '../ipc';
import * as fileSystem from './file-system';

import fs from 'fs';
import { generatePdf } from './export/pdf/generate-pdf';
import log from 'electron-log';

type SaveScriptOptions = EditorStatus & {
  suggestedDirectory: string;
  suggestedFilename: string;
}

export function openRequest(window: BrowserWindow) {
  asStrict(window.webContents).send('open:request');
}

async function handleOpenResponse(window: BrowserWindow, response: EditorStatus) {
  return replaceScript(window, response, async () => {
    await dialog.showOpenDialog(window, {
      defaultPath: fileSystem.DEFAULT_SCRIPT_DIRECTORY,
      properties: [
        // Allow files to be selected, but not directories
        'openFile'
      ],
      filters: [
        {
          name: 'ComicWriter script',
          extensions: [fileSystem.SCRIPT_EXTENSION]
        }
      ]
    })
      .then(result => {
        if (!result.canceled) {
          const filePath = result.filePaths[0];

          fileSystem.readScript(filePath)
            .then(script => {
              asStrict(window.webContents).send('open:result', {
                success: true,
                filePath,
                source: script.source
              });
            })
            .catch((error: Error) => {
              asStrict(window.webContents).send('open:result', {
                success: false,
                errorMessage: error.message,
                humanMessage: 'Unable to open script'
              });
            });
        }
      });
  });
}

export function saveAsRequest(window: BrowserWindow) {
  asStrict(window.webContents).send('save-as:request');
}

function handleSaveAsResponse(window: BrowserWindow, response: EditorStatus) {
  const suggestedDirectory = response.filePath != null
    ? path.dirname(response.filePath)
    : fileSystem.DEFAULT_SCRIPT_DIRECTORY;

  const suggestedFilename = response.filePath != null
    ? path.basename(response.filePath)
    : fileSystem.DEFAULT_SCRIPT_FILENAME;

  return performSave(window, {
    ...response,
    filePath: null,
    suggestedDirectory,
    suggestedFilename
  })
}

export function newScript(window: BrowserWindow) {
  asStrict(window.webContents).send('new:request');
}

export function saveRequest(window: BrowserWindow) {
  asStrict(window.webContents).send('save:request');
}

function handleNewResponse(window: BrowserWindow, response: EditorStatus) {
  return replaceScript(window, response, () => {
    asStrict(window.webContents).send('new:result');
  });
}

export function exportPdfRequest(window: BrowserWindow) {
  asStrict(window.webContents).send('export-pdf:request');
}

/**
 * Wraps some save logic around a function that replaces the editor's content.
 *
 * @param window
 * @param status
 * @param replace
 */
async function replaceScript(window: BrowserWindow, status: EditorStatus, replace: () => unknown) {
  if (!status.dirty) {
    return replace();
  } else {
    const result = await dialog.showMessageBox(window, {
      type: 'warning',
      buttons: [
        'Don\'t Save', 'Cancel', 'Save'
      ],
      defaultId: 2,
      cancelId: 1,
      message: 'Do you want to save the changes you made?',
      detail: 'Your changes will be lost if you don\'t save them.'
    });

    if (result.response === 0) {
      return replace();
    } else if (result.response === 2) {
      return handleSaveResponse(window, status)
        .then(result => {
          if (!result.canceled) replace();
        });
    }
  }
}

function handleSaveResponse(window: BrowserWindow, response: EditorStatus): Promise<SaveDialogReturnValue> {
  const suggestedDirectory = fileSystem.DEFAULT_SCRIPT_DIRECTORY;
  const suggestedFilename = fileSystem.DEFAULT_SCRIPT_FILENAME;

  return performSave(window, {
    ...response,
    suggestedDirectory,
    suggestedFilename
  });
}

async function handleExportPdfResponse(window: BrowserWindow, response: ExportPdfResponse) {
  const suggestedDirectory = response.filePath != null
    ? path.dirname(response.filePath)
    : fileSystem.DEFAULT_SCRIPT_DIRECTORY;

  const suggestedFilename = response.filePath != null
    ? fileSystem.asPdfFilename(response.filePath)
    : fileSystem.DEFAULT_PDF_FILENAME;

  const suggestedPath = path.join(suggestedDirectory, suggestedFilename);

  // always send null for current filepath to show the save as dialog
  const result = await ensureFilePath(window, null, suggestedPath);

  // if not canceled, save pdf to chosen location
  if (!result.canceled && result.filePath) {
    const pdfDoc = generatePdf(response.script);
    pdfDoc.pipe(fs.createWriteStream(result.filePath, {encoding: 'utf8'}));
    pdfDoc.end();
  }
}

async function performSave(window: BrowserWindow, options: SaveScriptOptions): Promise<SaveDialogReturnValue> {
  const defaultPath = path.join(options.suggestedDirectory, options.suggestedFilename);
  const currentFilePath = options.filePath;

  const result = await ensureFilePath(window, currentFilePath, defaultPath);
  const { canceled, filePath } = result;

  if (!canceled && filePath != null) {
    return fileSystem.writeScript(filePath, {source: options.source})
      .then(() => {
        asStrict(window.webContents).send('save:result', {
          success: true,
          filePath: filePath
        });
      })
      .then(() => result)
      .catch((error: Error) => {
        asStrict(window.webContents).send('save:result', {
          success: false,
          humanMessage: 'Unable to save script',
          errorMessage: error.message
        });

        // re-reject so downstream code can actually handle the error
        return Promise.reject(error);
      });
  } else {
    return result;
  }
}

/**
 * Prompt user to select a file path, if needed.
 *
 * @param window
 * @param filePath - The file path to use. Non-null if the path is already
 * known, null otherwise,
 * @param defaultPath - If filePath is null, this will be the default path of
 * the save dialog used to get a path from the user.
 */
async function ensureFilePath(window: BrowserWindow, filePath: string | null, defaultPath: string): Promise<SaveDialogReturnValue> {
  if (filePath != null) {
    return { canceled: false, filePath } as SaveDialogReturnValue;
  } else {
    return await dialog.showSaveDialog(window, {
      defaultPath
    });
  }
}


export function undo(window: BrowserWindow) {
  asStrict(window.webContents).send('undo');
}

export function redo(window: BrowserWindow) {
  asStrict(window.webContents).send('redo');
}

export function selectAll(window: BrowserWindow) {
  asStrict(window.webContents).send('selectAll');
}

export function openHelp() {
  shell.openExternal('https://gitlab.com/comic-writer/comic-writer-app?TODO=REPLACE_WITH_WEBSITE_HELP_PAGE')
    .catch(error => log.error(error));
}

// Handlers for responses sent from the renderer side

ipcMain.on('save:response', (event, response) => {
  const window = BrowserWindow.fromWebContents(event.sender);

  if (window) {
    handleSaveResponse(window, response)
      .catch(error => log.error(error));
  }
});

ipcMain.on('new:response', (event, response) => {
  const window = BrowserWindow.fromWebContents(event.sender);

  if (window) {
    handleNewResponse(window, response)
      .catch(error => log.error(error));
  }
});

ipcMain.on('save-as:response', (event, response) => {
  const window = BrowserWindow.fromWebContents(event.sender);

  if (window) {
    handleSaveAsResponse(window, response)
      .catch(error => log.error(error));
  }
});

ipcMain.on('open:response', (event, response) => {
  const window = BrowserWindow.fromWebContents(event.sender);

  if (window) {
    handleOpenResponse(window, response)
      .catch(error => log.error(error));
  }
});

ipcMain.on('export-pdf:response', (event, response) => {
  const window = BrowserWindow.fromWebContents(event.sender);

  if (window) {
    handleExportPdfResponse(window, response)
      .catch(error => log.error(error));
  }
});
