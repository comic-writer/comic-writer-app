import {
  Menu,
  BrowserWindow,
  MenuItemConstructorOptions,
} from 'electron';
import * as actions from './actions';

interface DarwinMenuItemConstructorOptions extends MenuItemConstructorOptions {
  selector?: string;
  submenu?: DarwinMenuItemConstructorOptions[] | Menu;
}

export default class MenuBuilder {
  mainWindow: BrowserWindow;

  constructor(mainWindow: BrowserWindow) {
    this.mainWindow = mainWindow;
  }

  buildMenu(): Menu {
    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      this.setupDevelopmentEnvironment();
    }

    const template =
      process.platform === 'darwin'
        ? this.buildDarwinTemplate()
        : this.buildDefaultTemplate();

    return Menu.buildFromTemplate(template);
  }

  setupDevelopmentEnvironment(): void {
    this.mainWindow.webContents.on('context-menu', (_, props) => {
      const { x, y } = props;

      Menu.buildFromTemplate([
        {
          label: 'Inspect element',
          click: () => {
            this.mainWindow.webContents.inspectElement(x, y);
          },
        },
      ]).popup({ window: this.mainWindow });
    });
  }

  /**
   * Create menu template to use on Macs.
   */
  buildDarwinTemplate(): MenuItemConstructorOptions[] {
    const subMenuAbout: DarwinMenuItemConstructorOptions = {
      label: 'ComicWriter',
      submenu: [
        { role: 'about', },
        { type: 'separator' },
        { role: 'services' },
        { type: 'separator' },
        { role: 'hide' },
        { role: 'hideOthers' },
        { role: 'unhide' },
        { type: 'separator' },
        { role: 'quit' },
      ],
    };

    const subMenuFile: DarwinMenuItemConstructorOptions = {
      label: 'File',
      submenu: [
        {
          label: 'New',
          accelerator: 'Command+N',
          click(_, window) {
            window && actions.newScript(window);
          },
        },
        { type: 'separator' },
        {
          label: 'Open...',
          accelerator: 'Command+O',
          click(_, window) {
            window && actions.openRequest(window);
          },
        },
        { type: 'separator' },
        {
          label: 'Save',
          accelerator: 'Command+S',
          click(_, window) {
            window && actions.saveRequest(window);
          },
        },
        {
          label: 'Save As...',
          accelerator: 'Shift+Command+S',
          click(_, window) {
            window && actions.saveAsRequest(window);
          },
        },
        { type: 'separator' },
        {
          label: 'Export as PDF...',
          click(_, window) {
            window && actions.exportPdfRequest(window);
          }
        }
      ]
    };

    const subMenuEdit: DarwinMenuItemConstructorOptions = {
      label: 'Edit',
      submenu: [
        {
          label: 'Undo',
          accelerator: 'Command+Z',
          click(_, window) {
            window && actions.undo(window);
          },
        },
        {
          label: 'Redo',
          accelerator: 'Shift+Command+Z',
          click(_, window) {
            window && actions.redo(window);
          },
        },
        { type: 'separator' },
        { role: 'cut' },
        { role: 'copy' },
        { role: 'paste' },
        {
          label: 'Select All',
          accelerator: 'Command+A',
          click(_, window) {
            window && actions.selectAll(window);
          },
        },
      ],
    };

    const subMenuViewDev: MenuItemConstructorOptions = {
      label: 'View',
      submenu: [
        { role: 'reload' },
        { role: 'togglefullscreen' },
        { role: 'toggleDevTools' },
      ],
    };

    const subMenuViewProd: MenuItemConstructorOptions = {
      label: 'View',
      submenu: [
        { role: 'togglefullscreen' },
      ],
    };

    const subMenuWindow: DarwinMenuItemConstructorOptions = {
      label: 'Window',
      submenu: [
        { role: 'minimize' },
        { role: 'close' },
        { type: 'separator' },
        { role: 'front' },
      ],
    };

    const subMenuHelp: MenuItemConstructorOptions = {
      label: 'Help',
      submenu: [
        {
          label: `ComicWriter Help`,
          click() {
            actions.openHelp();
          }
        }
      ],
    };

    const subMenuView =
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
        ? subMenuViewDev
        : subMenuViewProd;

    return [subMenuAbout, subMenuFile, subMenuEdit, subMenuView, subMenuWindow, subMenuHelp];
  }

  /**
   * Create menu template to use on everything *except* Macs.
   */
  buildDefaultTemplate(): MenuItemConstructorOptions[] {
    const templateDefault: MenuItemConstructorOptions[] = [
      {
        label: '&File',
        submenu: [
          {
            label: 'New',
            click(_, window) {
              window && actions.newScript(window);
            },
            accelerator: 'Ctrl+N'
          },
          { type: 'separator' },
          {
            label: 'Open...',
            click(_, window) {
              window && actions.openRequest(window);
            },
            accelerator: 'Ctrl+O'
          },
          { type: 'separator' },
          {
            label: 'Save',
            click(_, window) {
              window && actions.saveRequest(window);
            },
            accelerator: 'Ctrl+S'
          },
          {
            label: 'Save As...',
            click(_, window) {
              window && actions.saveAsRequest(window);
            },
            accelerator: 'Shift+Ctrl+S'
          },
          { type: 'separator' },
          {
            label: 'Export as PDF...',
            click(_, window) {
              window && actions.exportPdfRequest(window);
            }
          },
          { type: 'separator' },
          { role: 'close' },
        ],
      },
      {
        label: '&View',
        submenu:
          process.env.NODE_ENV === 'development' ||
          process.env.DEBUG_PROD === 'true'
            ? [
                { role: 'reload' },
                { role: 'togglefullscreen' },
                { role: 'toggleDevTools' },
              ]
            : [
                { role: 'togglefullscreen' },
              ],
      },
      {
        label: 'Help',
        submenu: [
          {
            label: 'Learn More',
            click() {
              actions.openHelp();
            },
          },
        ],
      },
    ];

    return templateDefault;
  }
}
