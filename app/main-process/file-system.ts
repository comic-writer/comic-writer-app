import fs from 'fs';
import path from 'path';
import { promisify } from 'util';
import { app } from 'electron';

const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);

const SCRIPT_ENCODING = 'utf8';

/** File extension for scripts. Note: This does not have a leading dot */
export const SCRIPT_EXTENSION = 'cwscript';

/** File extension for pdf files. Note: This does not have a leading dot */
const PDF_EXTENSION = 'pdf';

/** Absolute path of default directory for saving and opening scripts. */
export const DEFAULT_SCRIPT_DIRECTORY = app.getPath('documents');

/** Default filename for saving scripts. */
export const DEFAULT_SCRIPT_FILENAME = `untitled.${SCRIPT_EXTENSION}`;

/** Default filename for exporting a script as pdf. */
export const DEFAULT_PDF_FILENAME = `untitled.${PDF_EXTENSION}`;

type ScriptFile = {
  source: string;
}

export function writeScript(filePath: string, script: ScriptFile) {
  const data = stringifyScriptFile(script);
  return writeFile(filePath, data, {encoding: SCRIPT_ENCODING});
}

function stringifyScriptFile(script: ScriptFile): string {
  return JSON.stringify(script);
}

export function readScript(filePath: string) {
  return readFile(filePath, {encoding: SCRIPT_ENCODING})
    .then(data => parseScriptFile(data));
}

function parseScriptFile(data: string): ScriptFile {
  const obj: unknown = JSON.parse(data);

  if (validateParsedScriptFile(obj)) {
    return obj;
  } else {
    throw new Error('Invalid script file');
  }
}

// TODO find a cleaner way to do this
// if/when script files have more properties this needs to handle all possible
// schemas
function validateParsedScriptFile(obj: unknown): obj is ScriptFile {
  if (obj && typeof obj === 'object') {
    if ('source' in obj) {
      const withSource = obj as {source: unknown};
      return typeof withSource.source === 'string'
    }
  }

  return false;
}

export function asPdfFilename(scriptFilepath: string): string {
  return path.basename(scriptFilepath, path.extname(scriptFilepath)) + '.' + PDF_EXTENSION;
}
