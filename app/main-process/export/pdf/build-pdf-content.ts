import numberToWords from 'number-to-words';
import { Content, ContentText } from 'pdfmake/interfaces';
import {
  TextChunk,
  PreSpread,
  Paragraph,
  Metadata,
  Lettering,
  Sfx,
  Dialogue,
  Caption,
  Panel,
  SpreadChild,
  Spread,
  ComicNode,
} from '../../../parser/types';

import {
  FullScript,
} from '../../../script/types';
import * as headlineLevels from './headline-levels';
import * as parts from '../../../comic-part-types';
import {
  normalizePanelChildren,
  normalizePreSpread,
  normalizeSpreadChildren,
} from './normalize-script';
import { asMetadataMap } from '../helpers';

export function buildPdfContent(script: FullScript): Content {
  const content: Array<Content> = [
    ...renderPreSpread(script.preSpread)
  ];

  for (const spread of script.spreads) {
    content.push(...renderSpread(spread));
  }

  return content;
}

function renderPreSpread(preSpread: Array<PreSpread>): Array<Content> {
  const normalized = normalizePreSpread(preSpread);
  const {metadata, nodes} = extractTitleMetadata(normalized);

  return renderTitle(metadata).concat(
    nodes
      .map(node => {
        switch (node.type) {
          case parts.METADATA:
            return renderMetadata(node);
          case parts.PARAGRAPH:
            return renderParagraph(node);
          case parts.BLANK:
            return renderBlankLine();
        }
      })
      .map((node, index, array) => {
        const last = index === array.length - 1;
        return last ? lastPreSpread(node) : node;
      })
  );
}

function lastPreSpread(content: ContentText): ContentText {
  const oldStyle = content.style;
  const newStyle = typeof oldStyle === 'string' ? [oldStyle, 'lastPreSpread'] :
    Array.isArray(oldStyle) ? oldStyle.concat('lastPreSpread') : 'lastPreSpread';

  return {
    ...content,
    style: newStyle
  }
}

function renderTitle(metadata: Map<string, string>): Array<Content> {
  const content: Array<Content> = [];

  if (metadata.has('title')) {
    const issue = metadata.has('issue') ? ` #${metadata.get('issue') || ''}` : '';

    content.push({
      text: `${metadata.get('title') || ''}${issue}`,
      style: 'title'
    });

    if (metadata.has('by')) {
      content.push({
        text: `By ${metadata.get('by') || ''}`,
        style: 'by'
      });
    }

    content.push(renderBlankLine());
  }

  return content;
}

function extractTitleMetadata(nodes: Array<PreSpread>) {
  const metadataMap = asMetadataMap(nodes);

  return {
    metadata: metadataMap,
    nodes: nodes.filter(node => {
      if (metadataMap.has('title')) {
        return !(node.type === parts.METADATA
          && ['title', 'issue', 'by'].includes(node.name.toLocaleLowerCase()));
      } else {
        return true;
      }
    })
  };
}

function renderSpread(spread: Spread): Array<Content> {
  const content: Array<Content> = [];

  content.push({
    text: spreadTitle(spread),
    style: 'spread',
    headlineLevel: headlineLevels.SPREAD
  });

  content.push(...renderSpreadChildren(spread.children));

  return content;
}

function renderSpreadChildren(nodes: Array<SpreadChild>): Array<Content> {
  return normalizeSpreadChildren(nodes)
    .flatMap(node => {
      switch (node.type) {
        case parts.PARAGRAPH:
          return renderParagraph(node);
        case parts.BLANK:
          return renderBlankLine();
        case parts.PANEL:
          return renderPanel(node);
        case parts.DIALOGUE:
          return renderDialogue(node);
        case parts.CAPTION:
          return renderCaption(node);
        case parts.SFX:
          return renderSfx(node);
      }
    });
}

function spreadTitle(spread: Spread) {
  const pageNumbers = [];

  for (let i = 0; i < spread.pageCount; i++) {
    pageNumbers.push(spread.startPage + i);
  }

  return pageNumbers
    .map(number => numberToWords.toWords(number))
    .map(word => word.toUpperCase())
    .join(' & ');
}

function renderPanel(node: Panel): Array<Content> {
  const content: Array<Content> = [];

  const normalizedChildren = normalizePanelChildren(node.children);

  const firstChild = normalizedChildren[0];
  content.push({
    text: panelText(node, firstChild)
  });

  const childrenToRender = firstChild?.type === parts.PARAGRAPH
    ? normalizedChildren.slice(1)
    : normalizedChildren;

  for (const child of childrenToRender) {
    content.push(renderPanelChild(child));
  }

  // placeholder for panels with no lettering
  if (node.sfxCount + node.captionCount + node.dialogueCount === 0) {
    content.push(renderBlankLine());
    content.push(renderRawParagraph('NO COPY'));
  }

  return content;
}

function panelText(panel: Panel, firstChild?: ComicNode) {
  if (firstChild?.type === parts.PARAGRAPH) {
    return [
      {
        text: `${panel.label}: `,
        style: 'panel',
        headlineLevel: headlineLevels.PANEL
      },
      {
        text: boldableContentToText(firstChild.content)
      }
    ];
  } else {
    return {
      text: panel.label,
      style: 'panel',
      headlineLevel: headlineLevels.PANEL
    };
  }
}

function renderPanelChild(node: ComicNode): Content {
  switch (node.type) {
    case parts.PARAGRAPH:
      return renderParagraph(node);
    case parts.BLANK:
      return renderBlankLine();
    case parts.DIALOGUE:
      return renderDialogue(node);
    case parts.CAPTION:
      return renderCaption(node);
    case parts.SFX:
      return renderSfx(node);
    default:
      throw new Error(`Unsupported node type: ${node.type}`);
  }
}

function renderBlankLine(): ContentText {
  return {
    text: '\n'
  };
}

function renderParagraph(node: Paragraph): ContentText {
  return {
    text: boldableContentToText(node.content)
  };
}

function renderRawParagraph(text: string): ContentText {
  return {
    text
  };
}

function renderMetadata(node: Metadata): ContentText {
  return {
    text: `${node.name}: ${node.value}`
  };
}

function renderLettering(subject: string, node: Lettering): Content {
  const modifier = node.modifier ? ` (${node.modifier})` : '';

  return {
    layout: 'noBorders',
    table: {
      widths: [200, '*'],
      dontBreakRows: true,
      body: [
        [
          {
            text: `${node.number}. ${subject}${modifier}:`
          },
          {
            text: boldableContentToText(node.content)
          }
        ]
      ]
    }
  };
}

function boldableContentToText(chunks: Array<TextChunk>) {
  return chunks.map(chunk => ({
    text: chunk.content,
    style: chunk.type === parts.BOLD_TEXT ? 'bold' : undefined
  }));
}

function renderDialogue(node: Dialogue): Content {
  return renderLettering(node.speaker, node);
}

function renderCaption(node: Caption): Content {
  return renderLettering('CAPTION', node);
}

function renderSfx(node: Sfx): Content {
  return renderLettering('SFX', node);
}
