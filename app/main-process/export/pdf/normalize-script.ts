import {
  BlankLine,
  PreSpread,
  SpreadChild,
  PanelChild,
  ComicNode,
} from '../../../parser/types';
import * as parts from '../../../comic-part-types';

const DUMMY_BLANK: BlankLine = {
  type: parts.BLANK,
  id: 'blank',
  lineNumber: 0
};

/**
 * A node and count of blank lines between the node and the previous node.
 */
type BlankCount<T> = {
  node: T;
  blanksBefore: number;
};

function withBlankCounts<T extends ComicNode>(nodes: Array<T>) {
  const counts: Array<BlankCount<T>> = [];

  let consecutiveBlanks = 0;

  for (const node of nodes) {
    if (node.type === parts.BLANK) {
      consecutiveBlanks += 1;
    } else  {
      counts.push({
        node,
        blanksBefore: consecutiveBlanks
      });

      consecutiveBlanks = 0;
    }
  }

  return counts;
}

export function normalizePreSpread(nodes: Array<PreSpread>): Array<PreSpread> {
  return withBlankCounts(nodes)
    .flatMap((count, index, array) => {
      const currentNode = count.node;
      const previousCount = index === 0 ? null : array[index - 1];
      const previousNode = previousCount?.node;

      let blankBefore = false;

      switch (currentNode.type) {
        case parts.METADATA: {
          blankBefore = previousNode?.type === parts.PARAGRAPH;
          break;
        }
        case parts.PARAGRAPH: {
          if (previousNode?.type === parts.PARAGRAPH) {
            blankBefore = count.blanksBefore > 0;
          } else {
            blankBefore = !!previousNode;
          }
          break;
        }
      }

      return blankBefore ? [DUMMY_BLANK, count.node] : [count.node];
    })
}

export function normalizeSpreadChildren(nodes: Array<SpreadChild>): Array<SpreadChild> {
  return withBlankCounts(nodes)
    .flatMap((count, index, array) => {
      const currentNode = count.node;
      const previousCount = index === 0 ? null : array[index - 1];
      const previousNode = previousCount?.node;

      let blankBefore = false;

      switch (currentNode.type) {
        case parts.PARAGRAPH: {
          if (previousNode?.type === parts.PARAGRAPH) {
            blankBefore = count.blanksBefore > 0;
          } else if (previousNode && parts.isLettering(previousNode.type)) {
            blankBefore = true;
          }
          break;
        }
        case parts.CAPTION: // falls thru
        case parts.DIALOGUE: // falls thru
        case parts.SFX: {
          blankBefore = true;
          break;
        }
        case parts.PANEL: {
          // put blank before panel unless it's the first child of a spread
          blankBefore = !!previousNode;
          break;
        }
      }

      return blankBefore ? [DUMMY_BLANK, currentNode] : [currentNode];
    });
}

export function normalizePanelChildren(nodes: Array<PanelChild>): Array<PanelChild> {
  return withBlankCounts(nodes)
    .flatMap((count, index, array) => {
      const currentNode = count.node;
      const previousCount = index === 0 ? null : array[index - 1];
      const previousNode = previousCount?.node;

      let blankBefore = false;

      switch (currentNode.type) {
        case parts.PARAGRAPH: {
          if (previousNode?.type === parts.PARAGRAPH) {
            blankBefore = count.blanksBefore > 0;
          } else if (previousNode && parts.isLettering(previousNode.type)) {
            blankBefore = true;
          }
          break;
        }
        case parts.CAPTION: // falls thru
        case parts.DIALOGUE: // falls thru
        case parts.SFX: {
          blankBefore = true;
          break;
        }
      }

      return blankBefore ? [DUMMY_BLANK, currentNode] : [currentNode];
    });
}
