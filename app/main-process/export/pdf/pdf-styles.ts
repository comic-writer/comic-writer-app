import { StyleDictionary } from 'pdfmake/interfaces';

/*
Sizes are in points, 1 point == 1/72 inch

https://pdfmake.github.io/docs/0.1/document-definition-object/styling/
*/

export function styles(): StyleDictionary {
  return {
    // title at top of script: My Comic #1
    title: {
      fontSize: 20,
      bold: true,
      alignment: 'center',
      decoration: 'underline'
    },
    // line right below title: By <writer name>
    by: {
      alignment: 'center'
    },
    // add some bottom margin to the last pre-spread node to create some space
    // line between it and the first spread
    lastPreSpread: {
      // [left, top, right, bottom]
      margin: [0, 0, 0, 12]
    },
    spread: {
      fontSize: 20,
      bold: true
    },
    panel: {
      bold: true
    },
    bold: {
      bold: true
    },
    // text at top of every page except page 1
    header: {
      color: '#666666'
    },
  };
}

export function defaultStyle() {
  return {
    font: 'Helvetica',
    lineHeight: 1.4,
    fontSize: 12
  };
}
