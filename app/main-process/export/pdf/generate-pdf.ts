import PdfPrinter from 'pdfmake';
import { TDocumentDefinitions } from 'pdfmake/interfaces';
import { FullScript } from '../../../script/types';
import { buildPdfContent } from './build-pdf-content';
import { defaultStyle, styles } from './pdf-styles';
import { createHeader } from './header';
import * as headlineLevels from './headline-levels';

export function generatePdf(script: FullScript) {
  const printer = new PdfPrinter(fonts());
  const docDefinition = createDocDefinition(script);

  return printer.createPdfKitDocument(docDefinition);
}

function createDocDefinition(script: FullScript): TDocumentDefinitions {
  let spreadCount = 0;

  return {
    content: buildPdfContent(script),
    defaultStyle: defaultStyle(),
    header: createHeader(script),
    pageMargins: 72,
    styles: styles(),
    pageBreakBefore(node, _followingNodesOnPage, _nodesOnNextPage, previousNodesOnPage) {
      if (node.headlineLevel === headlineLevels.SPREAD) {
        spreadCount += 1;

        if (spreadCount === 1 && node.startPosition.pageNumber === 1) {
          // break if first spread has been pushed more than 1/2 down page 1
          return node.startPosition.verticalRatio > 0.5;
        } else {
          // break if spread isn't first thing on page
          return previousNodesOnPage.length > 0;
        }
      }

      return false;
    }
  };
}

function fonts() {
  return {
    Courier: {
      normal: 'Courier',
      bold: 'Courier-Bold',
      italics: 'Courier-Oblique',
      bolditalics: 'Courier-BoldOblique'
    },
    Helvetica: {
      normal: 'Helvetica',
      bold: 'Helvetica-Bold',
      italics: 'Helvetica-Oblique',
      bolditalics: 'Helvetica-BoldOblique'
    },
    Times: {
      normal: 'Times-Roman',
      bold: 'Times-Bold',
      italics: 'Times-Italic',
      bolditalics: 'Times-BoldItalic'
    },
    Symbol: {
      normal: 'Symbol'
    },
    ZapfDingbats: {
      normal: 'ZapfDingbats'
    }
  };
}
