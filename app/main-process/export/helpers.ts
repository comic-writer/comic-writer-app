import { FullScript } from '../../script/types';
import { Metadata, PreSpread } from '../../parser/types';
import * as parts from '../../comic-part-types';

/**
 * @returns Text to use in page header or null if there is none
 */
export function headerText(script: FullScript) {
  const metadataMap = asMetadataMap(script.preSpread);

  const title = metadataMap.get('title') || '';
  const issue = metadataMap.get('issue') || '';
  const by = metadataMap.get('by') || '';

  let text = '';
  if (title) {
    text = title;

    if (issue) {
      text += ` #${issue}`
    }
  }

  if (by) {
    if (text) {
      text += ` / ${by}`;
    } else {
      text = by;
    }
  }

  return text || null;
}

export function asMetadataMap(preSpread: Array<PreSpread>): Map<string, string> {
  return preSpread
    .filter((node): node is Metadata => node.type === parts.METADATA)
    .reduce((map, meta) => {
      map.set(meta.name.toLocaleLowerCase(), meta.value);
      return map;
    }, new Map<string, string>());
}
