import { Menu, App, BrowserWindow } from 'electron';
import MenuBuilder from './menu';
import packageJson from '../../package.json';

export function setup(app: App, mainWindow: BrowserWindow) {
  // Set up application menu bar (File, Edit, etc)
  const builder = new MenuBuilder(mainWindow);
  Menu.setApplicationMenu(builder.buildMenu());

  // Used in the default About popup
  app.setAboutPanelOptions({
    applicationName: 'ComicWriter',
    applicationVersion: packageJson.version,
    version: packageJson.version,
    copyright: '© 2020',
  });
}
