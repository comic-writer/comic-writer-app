import { TextEditorCommand } from './editor/types'
import { platformSpecific } from './utils/platform';

interface Hotkey {
  name: string;
  hotkey: string;
  inToolbar: boolean;
  command: TextEditorCommand;
}

export const Hotkeys: Array<Hotkey> = [
  {
    name: 'Page',
    hotkey: platformSpecific('Meta-1'),
    inToolbar: true,
    command: 'insertPage'
  },
  {
    name: 'Panel',
    hotkey: platformSpecific('Meta-2'),
    inToolbar: true,
    command: 'insertPanel'
  },
  {
    name: 'Balloon',
    hotkey: platformSpecific('Meta-3'),
    inToolbar: true,
    command: 'dialogue'
  },
  {
    name: 'Caption',
    hotkey: platformSpecific('Meta-4'),
    inToolbar: true,
    command: 'caption'
  },
  {
    name: 'SFX',
    hotkey: platformSpecific('Meta-5'),
    inToolbar: true,
    command: 'sfx'
  },
  {
    name: 'Bold',
    hotkey: platformSpecific('Meta-B'),
    inToolbar: true,
    command: 'bold'
  },
]
