import {
  observable,
  computed,
  action,
  decorate,
  IComputed,
} from 'mobx';

import {
  PreSpread,
  SpreadLines,
  Spread,
  SpreadChild,
  Metadata,
  Leaf,
} from '../parser/types';
import { LineStream } from './line-stream';
import { parsePreSpreadLines, parseSpreadLines } from '../parser';
import * as iterators from './iterator';
import * as parts from '../comic-part-types';

import {
  PanelCount,
  WordCount,
  FullScript,
} from './types';
import * as deepEquals from './deep-equals';

/**
 *
 * Important: Anything in this class that should be enhanced by mobx must also
 * be part of the decorate block below the class definition.
 */
export class ScriptStore {
  /**
   * Raw script content.
   */
  source: string;
  preSpread: Array<string>;
  spreads: Array<SpreadLines>;
  // private helpers functions with special initialization
  private spreadParser: (input: Array<SpreadLines>) => Array<Spread>;

  constructor() {
    this.source = '';
    this.preSpread = [];
    this.spreads = [];

    this.spreadParser = createMemoizedMapper<SpreadLines, Spread>(
      rawChunk => parseSpreadLines(rawChunk)
    );
  }

  // computed values

  get parsedSpreads(): Array<Spread> {
    return this.spreadParser(this.spreads);
  }

  get parsedPreSpreadNodes(): Array<PreSpread> {
    return parsePreSpreadLines(this.preSpread);
  }

  get preSpreadLineCount(): number {
    return this.preSpread.length;
  }

  get locatedSpreads(): Array<Spread> {
    let pageNumber = 1;
    const lineNumberer = createLineNumberer(this.preSpreadLineCount);
    const nodeIds = createNodeIds();

    // Because this is a 2-way generator, we need to "start" it
    nodeIds.next();

    return this.parsedSpreads.map(spread => {
      const locatedSpread = toLocatedSpread(spread, pageNumber, lineNumberer, nodeIds);

      // advance page number to next available page
      pageNumber += spread.pageCount;

      return locatedSpread;
    });
  }

  get locatedPreSpreadNodes(): Array<PreSpread> {
    const lineNumberer = createLineNumberer(0);
    const nodeIds = createNodeIds('pre-');

    return this.parsedPreSpreadNodes
      .map(node => toLocatedLeaf(node, lineNumberer, nodeIds));
  }

  get fullScript(): FullScript {
    return {
      preSpread: this.locatedPreSpreadNodes,
      spreads: this.locatedSpreads
    };
  }

  get speakers(): Array<string> {
    const speakers = new Set<string>();

    for (const spread of this.locatedSpreads) {
      for (const speaker of spread.speakers) {
        speakers.add(speaker.toUpperCase());
      }
    }

    return [...speakers].sort();
  }

  get panelCounts(): Array<PanelCount> {
    return this.locatedSpreads
      .map(spread => ({
        lineNumber: spread.lineNumber,
        count: spread.panelCount
      }));
  }

  get wordCounts(): Array<WordCount> {
    const wordCounts: Array<WordCount> = [];

    for (const node of iterators.spreadsAndChildren(this.locatedSpreads)) {
      if (node.type === parts.DIALOGUE || node.type === parts.CAPTION) {
        wordCounts.push({
          count: node.wordCount,
          lineNumber: node.lineNumber,
          isSpread: false
        });
      } else if (node.type === parts.PANEL || node.type === parts.SPREAD) {
        wordCounts.push({
          count: node.dialogueWordCount + node.captionWordCount,
          lineNumber: node.lineNumber,
          isSpread: node.type === parts.SPREAD
        });
      }
    }

    return wordCounts;
  }

  /**
   * All metadata found in the pre-spread lines.
   */
  get metadata(): Array<Metadata> {
    return this.parsedPreSpreadNodes
      .filter((node): node is Metadata => node.type == parts.METADATA);
  }

  // actions

  updateScript(script: Array<string> | string): void {
    const lines = Array.isArray(script)
      ? LineStream.fromLines(script)
      : LineStream.fromString(script);

    const incomingPreSpread = lines.consumeUntilSpreadStart();
    const incomingSpreads = lines.consumeAllSpreads();

    this.source = lines.toString();
    this.updatePreSpread(this.preSpread, incomingPreSpread);
    this.updateSpreads(this.spreads, incomingSpreads);
  }

  // private helpers

  private updatePreSpread(current: Array<string>, incoming: Array<string>): void {
    this.preSpread = deepEquals.strings(current, incoming)
      ? current
      : incoming;
  }

  private updateSpreads(current: Array<SpreadLines>, incoming: Array<SpreadLines>): void {
    let changes = 0;
    const nextSpreads: Array<SpreadLines> = [];

    const length = Math.max(current.length, incoming.length);
    for (let i = 0; i < length; i++) {
      const currentSpread = current[i];
      const incomingSpread = incoming[i];

      let nextSpread;

      if (deepEquals.spreadLines(currentSpread, incomingSpread)) {
        nextSpread = currentSpread;
      } else {
        nextSpread = incomingSpread;
        changes += 1;
      }

      if (nextSpread) {
        nextSpreads.push(nextSpread);
      }
    }

    if (changes > 0) {
      this.spreads = nextSpreads;
    }
  }
}

decorate(ScriptStore, {
  // observables
  source: observable,
  preSpread: observable.ref,
  spreads: observable.ref,

  // computeds
  parsedSpreads: computed,
  parsedPreSpreadNodes: computed,
  preSpreadLineCount: computed,
  locatedSpreads: computed,
  fullScript: computed,
  speakers: computed({
    equals: deepEquals.strings
  }) as IComputed,
  panelCounts: computed({
    equals: deepEquals.panelCounts,
  }) as IComputed,
  wordCounts: computed({
    equals: deepEquals.wordCounts
  }) as IComputed,
  metadata: computed,

  // actions
  updateScript: action,
});

function createMemoizedMapper<InputType, ResultType>(update: (i: InputType) => ResultType) {
  let lastInputs: Array<InputType> = [];
  let lastResults: Array<ResultType> = [];

  return function memoizedMapper(inputs: Array<InputType>): Array<ResultType> {
    const nextResults: Array<ResultType> = [];

    for (let i = 0; i < inputs.length; i++) {
      const currentInput = inputs[i];
      const lastInput = lastInputs[i];

      const nextResult = currentInput === lastInput
        ? lastResults[i]
        : update(currentInput);

      nextResults.push(nextResult);
    }

    lastInputs = inputs;
    lastResults = nextResults;

    return nextResults;
  };
}

function toLocatedSpread(
  spread: Spread,
  pageNumber: number,
  lineNumbers: Generator<number, void>,
  nodeIds: Generator<string, void, parts.COMIC_NODE>
): Spread {
  // This function uses Object.assign instead of object spread because the
  // object spread polyfill is slow and I can't figure out how to disable it

  return Object.assign(
    {},
    spread,
    {
      id: ensureValue(nodeIds.next(spread.type)),
      lineNumber: ensureValue(lineNumbers.next()),
      startPage: pageNumber,
      label: labelSpread(pageNumber, pageNumber + (spread.pageCount - 1)),
      children: spread.children.map(child => toLocatedSpreadChild(child, lineNumbers, nodeIds))
    }
  );
}

function toLocatedSpreadChild(
  child: SpreadChild,
  lineNumbers: Generator<number, void>,
  nodeIds: Generator<string, void, parts.COMIC_NODE>
): SpreadChild {
  // This function uses Object.assign instead of object spread because the
  // object spread polyfill is slow and I can't figure out how to disable it

  if (child.type === parts.PANEL) {
    // create located panel
    return Object.assign(
      {},
      child,
      {
        id: ensureValue(nodeIds.next(child.type)),
        lineNumber: ensureValue(lineNumbers.next()),
        label: labelPanel(child.number),
        children: child.children.map(panelChild => toLocatedLeaf(panelChild, lineNumbers, nodeIds))
      }
    );
  } else {
    // create located spread children (every child type except panel)
    return toLocatedLeaf(child, lineNumbers, nodeIds);
  }
}

function toLocatedLeaf<T extends Leaf>(
  leaf: T,
  lineNumbers: Generator<number, void>,
  nodeIds: Generator<string, void, parts.COMIC_NODE>
): T {
  return Object.assign(
    {},
    leaf,
    {
      id: ensureValue(nodeIds.next(leaf.type)),
      lineNumber: ensureValue(lineNumbers.next())
    }
  );
}

/**
 * Given the result returned by next() from a non-exhausted iterator, return the
 * value.
 *
 * @param result
 */
function ensureValue<YieldType, ReturnType>(result: IteratorResult<YieldType, ReturnType>): YieldType {
  if (!result.done) {
    return result.value;
  } else {
    throw new Error('Expected a non-done iterator result');
  }
}

function labelSpread(startPage: number, endPage: number): string {
  return startPage === endPage
    ? `Page ${startPage}`
    : `Pages ${startPage}-${endPage}`;
}

function labelPanel(number: number): string {
  return `Panel ${number}`;
}

function* createLineNumberer(start: number) {
  let number = start;
  while (true) {
    yield number++;
  }
}

function* createNodeIds(prefix = ''): Generator<string, void, parts.COMIC_NODE> {
  const generators = new Map<parts.COMIC_NODE, Generator<number, void>>();

  let nextType: parts.COMIC_NODE | null = null;

  while (true) {
    if (nextType) {
      if (!generators.has(nextType)) {
        generators.set(nextType, createIdGenerator());
      }

      const gen: Generator<number, void> = generators.get(nextType)!;
      nextType = yield `${prefix}${nextType}-${ensureValue(gen.next())}`;
    } else {
      nextType = yield 'dummy';
    }
  }
}

function* createIdGenerator() {
  let id = 1;
  while (true) {
    yield id++;
  }
}
