import { AppContainer as ReactHotAppContainer } from 'react-hot-loader';
import React, { Fragment } from 'react';
import { render } from 'react-dom';

import { ipcRenderer } from './ipc';
import { createStore, connectIpc } from './store';

import './index.css';

const store = createStore();
connectIpc(store, ipcRenderer);

const AppContainer = process.env.PLAIN_HMR ? Fragment : ReactHotAppContainer;

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line
  const Root = require('./containers/Root').default;
  render(
    <AppContainer>
      <Root store={store} />
    </AppContainer>,
    document.getElementById('root')
  );
});

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./containers/Root', () => {
    // eslint-disable-next-line
    const NextRoot = require('./containers/Root');
    render(
      <AppContainer>
        <NextRoot store={store} />
      </AppContainer>,
      document.getElementById('root')
    );
  });
}
