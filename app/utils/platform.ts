export const isMac = process.platform === 'darwin';

/**
 * Convert generic key combo into a platform-specific version.
 *
 * Example:
 *   Meta-1 => Cmd-1 or Ctrl-1
 *
 * @param keyCombo
 */
export function platformSpecific(keyCombo: string): string {
  return keyCombo.replace('Meta', isMac ? 'Cmd' : 'Ctrl');
}
