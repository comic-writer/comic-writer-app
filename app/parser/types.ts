import * as parts from '../comic-part-types';

/**
 * Nodes that can be in the upper script area before spreads.
 */
export type PreSpread = Metadata | Paragraph | BlankLine;

/**
 * Nodes that can be a child of a spread.
 */
export type SpreadChild = Panel | PanelChild;

/**
 * Nodes that can be a child of a panel.
 */
export type PanelChild = Lettering | Paragraph | BlankLine;

/** Any comic node that can appear anywhere in the script. */
export type ComicNode = Spread | Panel | Leaf;

/**
 * Nodes that don't contain other nodes.
 */
export type Leaf = Lettering | Paragraph | BlankLine | Metadata;

/** Lettering nodes */
export type Lettering = Dialogue | Caption | Sfx;

interface NodeIdentity {
  /**
   * Identifier for this node. AFter the script is numbered this will be unique
   * across all other nodes in the script.
   *
   * **Note:** This will have a dummy value until script is numbered.
   */
  id: string;
  /**
   * Zero based line number.
   *
   * **Note:** This will have a dummy value until script is numbered.
   */
  lineNumber: number;
}

export type Spread = {
  // core spread properties
  type: typeof parts.SPREAD;
  pageCount: number;
  children: Array<SpreadChild>;

  /**
   * Human-readadble label for this spread.
   *
   * **Note:** This will have a dummy value until script is numbered.
   */
  label: string;

  /**
   * One-based page number this spread starts on.
   *
   * **Note:** This will have a dummy value until script is numbered.
   */
  startPage: number;

  // properties that are derived from children
  panelCount: number;
  speakers: Array<string>;
  dialogueCount: number;
  captionCount: number;
  sfxCount: number;
  dialogueWordCount: number;
  captionWordCount: number;
} & NodeIdentity;

export type Panel = {
  // core panel properties
  type: typeof parts.PANEL;
  number: number;
  children: Array<PanelChild>;

  /**
   * Human-readable label for this panel.
   *
   * **Note:** This will have a dummy value until script is numbered.
   */
  label: string;

  // properties that are derived from children
  speakers: Array<string>;
  dialogueCount: number;
  captionCount: number;
  sfxCount: number;
  dialogueWordCount: number;
  captionWordCount: number;
  description: string | null;
} & NodeIdentity;

export type Paragraph = {
  type: typeof parts.PARAGRAPH;
  content: Array<TextChunk>;
} & NodeIdentity;

export type Metadata = {
  type: typeof parts.METADATA;
  name: string;
  value: string;
} & NodeIdentity;

export type Dialogue = {
  type: typeof parts.DIALOGUE;
  number: number;
  speaker: string;
  modifier: string | null;
  content: Array<TextChunk>;
  wordCount: number;
} & NodeIdentity;

export type Caption = {
  type: typeof parts.CAPTION;
  number: number;
  modifier: string | null;
  content: Array<TextChunk>;
  wordCount: number;
} & NodeIdentity;

export type Sfx = {
  type: typeof parts.SFX;
  number: number;
  modifier: string | null;
  content: Array<TextChunk>;
} & NodeIdentity;

export interface TextChunk {
  type: typeof parts.TEXT | typeof parts.BOLD_TEXT;
  content: string;
}

export type BlankLine = {
  type: typeof parts.BLANK;
} & NodeIdentity;

/**
 * The lines from a script that make up a spread and everything inside it.
 */
export interface SpreadLines {
  /**
   * The spread line.
   */
  spread: string;
  /**
   * The child lines of the spread.
   */
  children: Array<string>;
}
