import * as perf from '../perf';
import * as parts from '../comic-part-types';
import countWords from './count-words';
import * as classifiers from './line-classifiers';
import {
  Spread,
  Panel,
  Paragraph,
  Metadata,
  Dialogue,
  Caption,
  Sfx,
  TextChunk,
  BlankLine,
  PreSpread,
  SpreadChild,
  SpreadLines,
} from './types';

import {
  SPREAD_REGEX,
  PANEL_REGEX,
  CAPTION_REGEX,
  SFX_REGEX,
  DIALOGUE_REGEX,
  METADATA_REGEX,
  LETTERING_BOLD_REGEX
} from './regexes';

// flyweight pattern: reuse this to represent every blank line
const BLANK_LINE: BlankLine = {
  type: parts.BLANK,

  // dummy values for now, will be populated for real when script is numbered
  id: 'blank',
  lineNumber: 0,
};

export const parseSpreadLines = perf.wrap('parseSpreadLines', parse);

function parse(lines: SpreadLines): Spread {
  const spreadLine = lines.spread;
  const childLines = lines.children;

  const children = parseSpreadChildren(childLines);

  return parseSpread(spreadLine, children);
}

function parseSpreadChildren(lines: Array<string>): Array<SpreadChild> {
  const numbering = createLetteringNumberer();

  const spreadChildren: Array<SpreadChild> = [];
  let currentPanel: Panel | null = null;

  for (let i = 0; i < lines.length; i++) {
    const parsed = parseSpreadChild(lines[i], numbering);

    if (parsed.type === parts.PANEL) {
      currentPanel = parsed;
      spreadChildren.push(parsed);
    } else {
      if (currentPanel) {
        currentPanel.children.push(parsed);
      } else {
        spreadChildren.push(parsed);
      }
    }
  }

  return applyPanelRollups(spreadChildren);
}

function* createLetteringNumberer() {
  let number = 1;
  while (true) {
    yield number++;
  }
}

function applyPanelRollups(children: Array<SpreadChild>): Array<SpreadChild> {
  children.forEach(spreadChild => {
    if (spreadChild.type === parts.PANEL) {
      const panel = spreadChild;

      let letteringsSeen = 0;

      panel.children.forEach(panelChild => {
        switch (panelChild.type) {
          case parts.PARAGRAPH: {
            if (letteringsSeen === 0 && panel.description == null) {
              panel.description = paragraphText(panelChild);
            }
            break;
          }
          case parts.CAPTION: {
            panel.captionCount += 1;
            panel.captionWordCount += panelChild.wordCount;
            letteringsSeen += 1;
            break;
          }
          case parts.DIALOGUE: {
            panel.dialogueCount += 1;
            panel.dialogueWordCount += panelChild.wordCount;
            panel.speakers.push(panelChild.speaker);
            letteringsSeen += 1;
            break;
          }
          case parts.SFX: {
            panel.sfxCount += 1;
            letteringsSeen += 1;
            break;
          }
        }
      });
    }
  });

  return children;
}

function paragraphText(paragraph: Paragraph): string {
  return paragraph.content
    .map(chunk => chunk.content)
    .join('');
}

function parseSpreadChild(line: string, numbering: Generator<number>): SpreadChild {
  // scripts are about 1/2 blank lines so this should be first
  if (classifiers.isBlank(line)) return BLANK_LINE;

  if (classifiers.isCaption(line)) return parseCaption(line, numbering);
  if (classifiers.isSfx(line)) return parseSfx(line, numbering);
  // dialogue has to be checked after sfx/caption, otherwise we get balloons
  // where the speaker is "caption" and "sfx"
  if (classifiers.isDialogue(line)) return parseDialogue(line, numbering);

  if (classifiers.isPanel(line)) return parsePanel(line);

  // any non-blank line can be a paragraph so it goes last
  return parseParagraph(line);
}

export function parsePreSpreadLines(lines: Array<string>): Array<PreSpread> {
  return lines.map(line => parsePreSpreadLine(line));
}

function parsePreSpreadLine(line: string): PreSpread {
  // scripts are about 1/2 blank lines so this should be first
  if (classifiers.isBlank(line)) return BLANK_LINE;

  if (classifiers.isMetadata(line)) return parseMetadata(line);

  // any non-blank line can be a paragraph so it goes last
  return parseParagraph(line);
}

export function parseSpread(line: string, children: Array<SpreadChild>): Spread {
  const matchResult = SPREAD_REGEX.exec(line) as Array<string>;

  const startPage = Number(matchResult[1]);
  const endPage = matchResult[3] != null ? Number(matchResult[3]) : startPage;
  const pageCount = countPages(startPage, endPage);

  return applySpreadRollups({
    type: parts.SPREAD,
    pageCount,
    startPage,
    children,

    // dummy values for now, will be populated for real when script is numbered
    id: 'spread',
    lineNumber: 0,
    label: 'spread',

    // these start with default values, they're populated when applying rollups
    panelCount: 0,
    speakers: [],
    dialogueCount: 0,
    captionCount: 0,
    sfxCount: 0,
    dialogueWordCount: 0,
    captionWordCount: 0
  });
}

function applySpreadRollups(spread: Spread): Spread {
  return spread.children.reduce((spread, child) => {
    switch (child.type) {
      case parts.PANEL:
        spread.panelCount        += 1;
        spread.captionCount      += child.captionCount;
        spread.captionWordCount  += child.captionWordCount;
        spread.dialogueCount     += child.dialogueCount;
        spread.dialogueWordCount += child.dialogueWordCount;
        spread.sfxCount          += child.sfxCount;
        spread.speakers.push(...child.speakers);
        break;
      case parts.DIALOGUE:
        spread.dialogueCount     += 1;
        spread.dialogueWordCount += child.wordCount;
        break;
      case parts.CAPTION:
        spread.captionCount      += 1;
        spread.captionWordCount  += child.wordCount;
        break;
    }

    return spread;
  }, spread);
}

function countPages(startPage: number, endPage?: number): number {
  if (endPage == null) {
    return 1;
  } else if (startPage < endPage) {
    return (endPage - startPage) + 1;
  } else if (startPage > endPage) {
    return 2;
  } else { // startPage === endPage
    return 1;
  }
}

export function parsePanel(line: string): Panel {
  const [, number] = PANEL_REGEX.exec(line) as Array<string>;

  return {
    type: parts.PANEL,
    number: Number(number),

    // dummy values for now, will be populated for real when script is numbered
    id: 'panel',
    lineNumber: 0,
    label: 'panel',

    // these start with default values, they'll be populated later
    children: [],
    speakers: [],
    dialogueCount: 0,
    captionCount: 0,
    sfxCount: 0,
    dialogueWordCount: 0,
    captionWordCount: 0,
    description: null
  };
}

export function parseMetadata(line: string): Metadata {
  const [, name, value] = METADATA_REGEX.exec(line) as Array<string>;

  return {
    type: parts.METADATA,
    name,
    value,

    // dummy values for now, will be populated for real when script is numbered
    id: 'metadata',
    lineNumber: 0,
  };
}

export function parseParagraph(line: string): Paragraph {
  return {
    type: parts.PARAGRAPH,
    content: parseBoldableContent(line),

    // dummy values for now, will be populated for real when script is numbered
    id: 'paragraph',
    lineNumber: 0,
  };
}

export function parseDialogue(line: string, numbering: Generator<number, void>): Dialogue {
  const [, speaker, modifier, content] = DIALOGUE_REGEX.exec(line) as Array<string>;

  const parseTree = parseBoldableContent(content);

  return {
    type: parts.DIALOGUE,
    number: nextValue(numbering),
    speaker,
    modifier: normalizeModifier(modifier),
    content: parseTree,
    wordCount: countWords(parseTree),

    // dummy values for now, will be populated for real when script is numbered
    id: 'dialogue',
    lineNumber: 0,
  };
}

export function parseCaption(line: string, numbering: Generator<number, void>): Caption {
  const [, modifier, content] = CAPTION_REGEX.exec(line) as Array<string>;
  const parseTree = parseBoldableContent(content);

  return {
    type: parts.CAPTION,
    number: nextValue(numbering),
    modifier: normalizeModifier(modifier),
    content: parseTree,
    wordCount: countWords(parseTree),

    // dummy values for now, will be populated for real when script is numbered
    id: 'caption',
    lineNumber: 0,
  };
}

export function parseSfx(line: string, numbering: Generator<number, void>): Sfx {
  const [, modifier, content] = SFX_REGEX.exec(line) as Array<string>;

  return {
    type: parts.SFX,
    number: nextValue(numbering),
    modifier: normalizeModifier(modifier),
    content: parseBoldableContent(content),

    // dummy values for now, will be populated for real when script is numbered
    id: 'sfx',
    lineNumber: 0,
  };
}

function nextValue<TValue, TReturn>(gen: Generator<TValue, TReturn>): TValue {
  const next = gen.next();

  if (!next.done) {
    return next.value;
  } else {
    throw new Error('Expected a non-exhausted generator');
  }
}

function normalizeModifier(modifier: string | null): string | null {
  return modifier ? modifier.slice(1, -1) || null : null;
}

function parseBoldableContent(content: string): Array<TextChunk> {
  const chunks: Array<TextChunk> = [];

  let index = 0;
  let result = null;

  // eslint-disable-next-line no-cond-assign
  while (result = LETTERING_BOLD_REGEX.exec(content.slice(index))) {
    const before = content.slice(index, index + result.index)

    if (before) {
      chunks.push({
        type: parts.TEXT,
        content: before
      });
    }

    chunks.push({
      type: parts.BOLD_TEXT,
      content: result[1]
    })

    index += result.index + result[0].length;
  }

  const after = content.slice(index);
  if (after) {
    chunks.push({
      type: parts.TEXT,
      content: after
    });
  }

  return chunks;
}
