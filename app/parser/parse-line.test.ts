import {
  parseSpread,
  parsePanel,
  parseMetadata,
  parseParagraph,
  parseCaption,
  parseDialogue,
  parseSfx
} from './parse';

function* letteringNumbers(start = 1) {
  let number = start;
  while (true) {
    yield number++;
  }
}

describe('parse lines', () => {
  describe('parseSpread', () => {
    test('single page', () => {
      const result = parseSpread('Page 2', []);

      expect(result).toEqual({
        type: 'spread',
        pageCount: 1,
        children: [],

        id: 'spread',
        label: 'spread',
        lineNumber: 0,
        startPage: 2,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('double page', () => {
      const result = parseSpread('Pages 2-3', []);

      expect(result).toEqual({
        type: 'spread',
        pageCount: 2,
        children: [],

        id: 'spread',
        label: 'spread',
        lineNumber: 0,
        startPage: 2,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('3+ page range', () => {
      const result = parseSpread('Pages 3-6', []);

      expect(result).toEqual({
        type: 'spread',
        pageCount: 4,
        children: [],

        id: 'spread',
        label: 'spread',
        lineNumber: 0,
        startPage: 3,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('partial page range', () => {
      const result = parseSpread('Pages 2-', []);

      expect(result).toEqual({
        type: 'spread',
        pageCount: 1,
        children: [],

        id: 'spread',
        label: 'spread',
        lineNumber: 0,
        startPage: 2,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('case insensitive', () => {
      const result = parseSpread('PAGE 1', []);

      expect(result).toEqual({
        type: 'spread',
        pageCount: 1,
        children: [],

        id: 'spread',
        label: 'spread',
        lineNumber: 0,
        startPage: 1,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });
  });

  describe('parsePanel', () => {
    test('single', () => {
      const result = parsePanel('Panel 3');

      expect(result).toEqual({
        type: 'panel',
        number: 3,
        children: [],
        description: null,

        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0,

        id: 'panel',
        label: 'panel',
        lineNumber: 0,
      });
    });

    test('case insensitive', () => {
      const result = parsePanel('PANEL 3');

      expect(result).toEqual({
        type: 'panel',
        number: 3,
        children: [],
        description: null,

        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0,

        id: 'panel',
        label: 'panel',
        lineNumber: 0,
      });
    });
  });

  describe('parseMetadata', () => {
    test('basic key/value', () => {
      const result = parseMetadata('Issue: 4');

      expect(result).toEqual({
        type: 'metadata',
        name: 'Issue',
        value: '4',

        id: 'metadata',
        lineNumber: 0,
      });
    });

    test('multi word key/value', () => {
      const result = parseMetadata('Written by: First Last');

      expect(result).toEqual({
        type: 'metadata',
        name: 'Written by',
        value: 'First Last',

        id: 'metadata',
        lineNumber: 0,
      });
    });
  });

  describe('parseParagraph', () => {
    test('plain paragraph', () => {
      const result = parseParagraph('It was a good day.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'It was a good day.'
          }
        ],

        id: 'paragraph',
        lineNumber: 0,
      });
    });

    test('paragraph with a bold word', () => {
      const result = parseParagraph('It was a **good** day.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'It was a '
          },
          {
            type: 'bold-text',
            content: 'good'
          },
          {
            type: 'text',
            content: ' day.'
          }
        ],

        id: 'paragraph',
        lineNumber: 0,
      });
    });

    test('paragraph with multiple bold words', () => {
      const result = parseParagraph('It **was** a **good** day.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'It '
          },
          {
            type: 'bold-text',
            content: 'was'
          },
          {
            type: 'text',
            content: ' a '
          },
          {
            type: 'bold-text',
            content: 'good'
          },
          {
            type: 'text',
            content: ' day.'
          }
        ],

        id: 'paragraph',
        lineNumber: 0,
      });
    });

    test('mostly bold paragraph', () => {
      const result = parseParagraph('**It was** a **good day.**');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'bold-text',
            content: 'It was'
          },
          {
            type: 'text',
            content: ' a '
          },
          {
            type: 'bold-text',
            content: 'good day.'
          }
        ],

        id: 'paragraph',
        lineNumber: 0,
      });
    });

    test('entirely bold paragraph', () => {
      const result = parseParagraph('**It was a good day.**');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'bold-text',
            content: 'It was a good day.'
          }
        ],

        id: 'paragraph',
        lineNumber: 0,
      });
    });
  });

  describe('parseCaption', () => {
    test('basic', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tCAPTION: It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: null,
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: 'caption',
        lineNumber: 0,
      });
    });

    test('with bold', () => {
      const numbering = letteringNumbers(5);
      const result = parseCaption('\tCAPTION: It **was** a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 5,
        modifier: null,
        content: [{
          type: 'text',
          content: 'It '
        }, {
          type: 'bold-text',
          content: 'was'
        }, {
            type: 'text',
            content: ' a good idea.'
        }],
        wordCount: 5,

        id: 'caption',
        lineNumber: 0,
      });
    });

    test('with modifier', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tCAPTION (PERSON): It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: 'PERSON',
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: 'caption',
        lineNumber: 0,
      });
    });

    test('with empty modifier parens', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tCAPTION (): It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: null,
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: 'caption',
        lineNumber: 0,
      });
    });

    test('case insensitive', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tcaption: It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: null,
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: 'caption',
        lineNumber: 0,
      });
    });

  });

  describe('parseDialogue', () => {
    test('basic', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU: I thought it was a good idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: null,
        content: [{
          type: 'text',
          content: 'I thought it was a good idea.'
        }],
        wordCount: 7,

        id: 'dialogue',
        lineNumber: 0,
      });
    });

    test('with bold', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU: I thought it was a **good** idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: null,
        content: [{
          type: 'text',
          content: 'I thought it was a '
        }, {
          type: 'bold-text',
          content: 'good'
        }, {
          type: 'text',
          content: ' idea.'
        }],
        wordCount: 7,

        id: 'dialogue',
        lineNumber: 0,
      });
    });

    test('with modifier', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU (OFF): I thought it was a good idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: 'OFF',
        content: [{
          type: 'text',
          content: 'I thought it was a good idea.'
        }],
        wordCount: 7,

        id: 'dialogue',
        lineNumber: 0,
      });
    });

    test('with empty modifier parens', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU (): I thought it was a good idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: null,
        content: [{
          type: 'text',
          content: 'I thought it was a good idea.'
        }],
        wordCount: 7,

        id: 'dialogue',
        lineNumber: 0,
      });
    });
  });

  describe('parseSfx', () => {
    test('basic', () => {
      const numbering = letteringNumbers(2);
      const result = parseSfx('\tSFX: BLAM', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 2,
        modifier: null,
        content: [
          {
            type: 'text',
            content: 'BLAM'
          }
        ],

        id: 'sfx',
        lineNumber: 0,
      });
    });

    test('with modifier', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX (GLASS): clink', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: 'GLASS',
        content: [
          {
            type: 'text',
            content: 'clink'
          }
        ],

        id: 'sfx',
        lineNumber: 0,
      });
    });

    test('with empty modifier parens', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX (): clink', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: null,
        content: [
          {
            type: 'text',
            content: 'clink'
          }
        ],

        id: 'sfx',
        lineNumber: 0,
      });
    });

    test('case insensitive', () => {
      const numbering = letteringNumbers(12);
      const result = parseSfx('\tsfx: BLAM', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 12,
        modifier: null,
        content: [
          {
            type: 'text',
            content: 'BLAM'
          }
        ],

        id: 'sfx',
        lineNumber: 0,
      });
    });

    test('all bold', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX: **BLAM**', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: null,
        content: [
          {
            type: 'bold-text',
            content: 'BLAM'
          }
        ],

        id: 'sfx',
        lineNumber: 0,
      });
    });

    test('partial bold', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX: BA-**BOOM**', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: null,
        content: [
          {
            type: 'text',
            content: 'BA-'
          },
          {
            type: 'bold-text',
            content: 'BOOM'
          }
        ],

        id: 'sfx',
        lineNumber: 0,
      });
    });
  });
});