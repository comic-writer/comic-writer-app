/* eslint react/jsx-props-no-spreading: off */
import React from 'react';
import { Switch, Route } from 'react-router-dom';

import App from './containers/App';
import { Writer } from './containers/Writer';

export default function Routes() {
  return (
    <App>
      <Switch>
        <Route path="/" component={Writer} />
      </Switch>
    </App>
  );
}
