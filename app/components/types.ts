import { ReactNode } from 'react';

export interface WithReactChildren {
  /** Child content */
  children: ReactNode;
}
